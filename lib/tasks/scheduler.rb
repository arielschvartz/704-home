namespace :scheduler do
  desc "This task is called by the Heroku scheduler add-on"

  task :cart_cleanup => :environment do
    puts "Starting Carts cleanup..."
    Cart.cleanup
    puts "done."
  end
end
