namespace :scheduler do
  desc "This task is called by the Heroku scheduler add-on"

  task :cart_cleanup => :environment do
    puts "Starting Carts cleanup..."
    Cart.where(['updated_at < ? AND user_id IS NULL', 24.hours.ago]).destroy_all
    puts "done."
  end
end
