Site704home::Application.routes.draw do
  get "coupons/create"

	devise_for :admin_users, ActiveAdmin::Devise.config
	ActiveAdmin.routes(self)

	devise_for :users,
	path: "",
	path_names: {
		sign_in: "login",
		sign_out: "logout",
	},
	controllers: { omniauth_callbacks: "users/omniauth_callbacks", registrations: "users/registrations", sessions: "users/sessions", passwords: "users/passwords"  }

	resources :product_categories, only: [:index, :show], path: 'categorias'
	resources :ambients, only: [:index, :show], path: 'ambientes' do
		resources :sub_ambients, only: [:show], path: 'subambientes'
	end

	resources :users, only: [], path: 'usuarios' do
		member do
			get 'cart', controller: 'carts', action: 'show', path: 'carrinho'
			get 'carts', controller: 'carts', action: 'index', path: 'pedidos'
			# get 'payments', controller: 'payments', action: 'index', path: 'pagamentos'
		end
	end

	resources :payments, only: [:new, :create], path: 'pagamentos'
	resources :payment_notifications, only: [:create], path: 'notificacoes_de_pagamento'

	resources :newsletters, only: [:create]

	# resources :magazines, only: [:show, :index], path: 'revistas'
	resources :magazines, only: [:show], path: 'revistas'

	resources :products, only: [], path: 'busca' do
		collection do
			get 'search'
			get 'color_search'
		end
	end

	resources :color_products, only: [:show], path: 'produtos' do
		resources :cart_color_products, only: [:index, :create, :update, :destroy]
	end

	resources :post_categories, only: [:show]
	resources :posts, only: [:show]

	resources :year, only: [] do
		resources :month, only: [] do
			resources :calendar, only: [:show]
		end
	end

	resources :architects, only: [:show, :index], path: 'arquitetos'

	resources :carts, only: [:show], path: 'carrinho' do
		member do
			resources :coupons, only: [:create], path: 'cupons'
		end
	end

	get 'quem_somos', controller: 'static_pages', action: 'about', path: 'quem_somos', as: :about
	get 'termos_de_privacidade', controller: 'static_pages', action: 'privacy', path: 'termos_de_privacidade', as: :privacy
	get 'politica_de_troca', controller: 'static_pages', action: 'trade', path: 'politica_de_troca', as: :trade
	get 'clube', controller: 'static_pages', action: 'club', path: 'clube', as: :club
	get 'faq', controller: 'static_pages', action: 'faq', path: 'faq', as: :faq
	get 'regulamento_promocional', controller: 'static_pages', action: 'promotion_rules', as: :promotion_rules

	match 'contact' => 'contact#create', as: :contact, via: :post
	resource :comments, only: [:create]

	resources :collections, only: [:show], path: 'colecoes'

	resources :multibrands, only: [:new, :create], path: 'multimarcas'
	resources :b2bs, only: [:new, :create], path: 'b2b'
	resources :presses, only: [:index, :show], path: '704-na-midia'

	# resources :magazine_pages, only: [:create]

	root to: 'static_pages#home'
	# root to: 'static_pages#building'
	match 'home' => 'static_pages#home', as: :home, via: :get

	get 'sitemap' => 'sitemap#index', as: :sitemap

	get 'obrigado' => 'static_pages#home', as: :thanks, via: :get

	resources :shipping, only: [:create], controller: 'carts/shipping'

	namespace :api do
		namespace :v1 do
			resources :products, only: [:create, :update, :index, :show]
			resources :colors, only: [:create, :update, :index, :show]
			resources :color_products, only: [:create, :update, :index, :show]
			resources :payments, only: [:index, :show] do
				member do
					put 'sync'
					put 'set_shipping_code'
				end
				collection do
					get 'no_shipping_code'
				end
			end
		end
	end
end
