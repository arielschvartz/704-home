String.prototype.toCamelCase = ->
  parts = this.split('_')
  final_parts = []

  for part in parts
    final_parts.push(part.slice(0,1).toUpperCase() + part.slice(1).toLowerCase())

  final = final_parts.join('')
  final = final.slice(0,1).toLowerCase() + final.slice(1)
  final

INSTALLMENT_RESPONSE = null

$ ->
  id = $('#pagseguro-session-id').data('session-id')
  response = PagSeguroDirectPayment.setSessionId(id)

  $('form#new_payment').find('.buy_bt.yellow-button').click (e) ->
    e.preventDefault()
    e.stopPropagation()

    sender_hash = PagSeguroDirectPayment.getSenderHash()
    # console.log sender_hash
    $('#payment_sender_hash').val(sender_hash)
    $('form#new_payment').submit()


  $('.credit_card_info').find('input[type="text"]').keyup ->
    update_card()
  $('.credit_card_info').find('select').change ->
    update_card()

  $('select#payment_payment_method').change ->
    if $(this).val() == 'creditCard'
      $('.credit_card_inputs').slideDown('fast')
    else
      $('.credit_card_inputs').slideUp('fast')

  $('#payment_billing_same_as_shipping_input').find('input').change ->
    if $(this).is(':checked')
      $('.billing_address_field').slideUp('fast')
    else
      $('.billing_address_field').slideDown('fast')

  update_card()


  PagSeguroDirectPayment.getInstallments
    amount: $('#cart-total').data('cart-total')
    maxInstallmentNoInterest: 3
    success: (response) ->
      INSTALLMENT_RESPONSE = response
      update_installment_value()
    error: (response) ->

  $("#payment_no_interest_installment_quantity").change ->
    update_installment_value()


update_installment_value = ->
  amount = parseInt($("#payment_no_interest_installment_quantity").val())
  unless amount == '' or amount == undefined or amount == null or INSTALLMENT_RESPONSE == null
    $('#payment_installment_value').val(INSTALLMENT_RESPONSE.installments.visa[amount - 1].installmentAmount)

update_card = ->
  if card_info_complete()
    params = {}
    # for id in ['payment_card_number', 'payment_brand', 'payment_cvv', 'payment_expiration_month', 'payment_expiration_year']
    for id in ['payment_card_number', 'payment_cvv', 'payment_expiration_month', 'payment_expiration_year']
      if id == 'payment_brand'
        val = $('.credit_card_info').find('input[type="radio"]:checked').val()
      else
        val = $('.credit_card_info').find("##{id}").val()

      params[id.split('payment_')[1].toCamelCase()] = val

    PagSeguroDirectPayment.createCardToken $.extend params,
      success: (response) ->
        token = response.card.token
        $('#payment_credit_card_token').val(token)
        # console.log $('#payment_credit_card_token').val()
      error: (response) ->
        alert 'Cartão de Crédito inválido'

set_payment_method_options = (options) ->
  select = $('select#payment_payment_method')

set_card_brands_options = (options) ->
  choice_group = $('li.input#payment_brand_input').find('ol.choices-group')
  for k, v of options
    add_option_to_select choice_group, v

add_option_to_select = (choice_group, brand) ->
  li = $('<li class="choice">')
  label = $("<label for='payment_brand_#{brand.name}'>")
  input = $("<input id='payment_brand_#{brand.name}' name='payment[brand]' type='radio' value='#{brand.name}'>")
  img = $("<img src='https://stc.pagseguro.uol.com.br#{brand.images.SMALL.path}' title='#{brand.displayName}'>")

  li.append label
  label.append input
  label.append img
  choice_group.append li

card_info_complete = ->
  # for id in ['payment_card_number', 'payment_brand', 'payment_cvv', 'payment_expiration_month', 'payment_expiration_year']
  for id in ['payment_card_number', 'payment_cvv', 'payment_expiration_month', 'payment_expiration_year']
    if id == 'payment_brand'
      val = $('.credit_card_info').find('input[type="radio"]:checked').val()
    else
      val = $('.credit_card_info').find("##{id}").val()

    # console.log val
    if val == '' or val == undefined or val == null
      return false

  true
