$ ->
  $('#payment_shipping_address_postal_code').focusout ->
    $.ajax
      url: '/shipping'
      method: 'POST'
      data:
        cart:
          shipping_zip_code: $(this).val()
      success: (data, textStatus, jqXHR) ->
        if data.status == 'error'
          alert data.message
          $('#payment_shipping_address_postal_code').val('')
        else
          $('#shipping-cost').find('.total_price').text(data.shipping.value)
          $('#cart-total-price').find('.total_price').text(data.cart.total)
          alert 'Valor do Frete atualizado com sucesso.'

