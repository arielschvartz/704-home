root = exports ? this

# FLASH MESSAGES

HIDE_DELAY = 8000

root.flash = (msg, type = 'error', elem = null) ->
	div = $('<div>').addClass("flash_message flash_#{type}").text(msg)
	if elem == null
		$('#container').prepend div
	else
		elem.prepend div
	hide div

root.flash_on_register_modal = (errors, params) ->
	flash = () ->
		html_elem = $('.modal-body').find('#new_user:first')
		for key, value of errors
			html_elem.find('#' + 'user' + '_' + key).addClass 'error_field'
			root.flash "#{root.translate key} #{value.toString().split(',')[0]}", 'error', html_elem
		for key, value of params
			html_elem.find('#' + 'user' + '_' + key).val(value)
		$('.modal-render-link:last', '.secondary-login-links').unbind 'ajax:success', flash

	$('.modal-render-link:last', '.secondary-login-links').click()
	$('.modal-render-link:last', '.secondary-login-links').bind 'ajax:success', flash

root.clear_flash = () ->
	$('.error_field').removeClass 'error_field'
	$('.flash_error').css 'display', 'none'

root.hide = (elem, miliseconds = HIDE_DELAY) ->
	unless elem.hasClass('no-hide')
		elem.delay(miliseconds).slideUp('fast')

$ ->
	$('.flash_message').each () ->
		hide $(this)

# REDIRECT

root.redirect = (link) ->
	window.location = link

# EXTRA

root.delay = (ms, func) ->
	setTimeout(func,  ms)
