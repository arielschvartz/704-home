root = exports ? this

root.translate = (word) ->
  switch word
    when "email" then returned_word = "e-mail"
    when "password" then returned_word = "senha"
    when "email_or_password" then returned_word = "e-mail ou senha"
    when "name" then returned_word = "nome"
    when "login" then returned_word = "Login"
    when "cpf" then returned_word = "CPF"
  return returned_word