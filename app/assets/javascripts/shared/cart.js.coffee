root = exports ? this

$ ->
	$('.add_to_cart').bind 'ajax:success',  (evt, data, status, xhr) ->
		if data.status
			if data.redirect
				window.location.href = data.link
			else
				root.flash 'Produto adicionado ao carrinho com sucesso.', 'success'
		else
			if data.errors.hasOwnProperty('login')
				root.clear_flash()
				root.flash_on_register_modal(data.errors, data.params)
			else
				root.clear_flash()
				root.flash 'Ocorreu um erro.'
	$('.add_to_cart').bind 'ajax:error', (evt, xhr, status, error) ->
		root.clear_flash()
		root.flash 'Ocorreu um erro.'

	$('.product_body').find('.buy_bt').bind 'ajax:success',  (evt, data, status, xhr) ->
		if data.status
			if data.redirect
				window.location.href = data.link
			else
				root.flash 'Produto adicionado ao carrinho com sucesso.', 'success'
		else
			if data.errors.hasOwnProperty('login')
				root.clear_flash()
				root.flash_on_register_modal(data.errors, data.params)
			else
				root.clear_flash()
				root.flash 'Ocorreu um erro.'
	$('.product_body').find('.buy_bt').bind 'ajax:error', (evt, xhr, status, error) ->
		root.clear_flash()
		root.flash 'Ocorreu um erro.'

	$('.delete_from_cart').bind 'ajax:success', (evt, data, status, xhr) ->
		if data.status
			if data.redirect
				window.location.href = data.link
			else
				root.flash 'Produto removido com sucesso.', 'success'
				$('[data-id=' + data.id + ']').remove()
				$('.total_price').text(($('.total_price').text().split(' ')[0] + ' ' + data.price))

	$('.delete_from_cart').bind 'ajax:error', (evt, xhr, status, error) ->
		root.clear_flash()
		root.flash 'Ocorreu um erro.'

	$('.ammount_select').find('.option').click (event) ->
		event.stopPropagation()
		ammount = parseInt($(this).text())
		old_url = $('.add_to_cart').prop 'href'
		new_url = old_url.split('?')[0] + "?ammount=#{ammount}"
		$('.add_to_cart').prop 'href': new_url
		$('.product_body').find('.buy_bt').prop 'href': new_url



$ ->
	$('#buy-bt-form').on 'ajax:success',  (evt, data, status, xhr) ->
		root.clear_flash()
		if data.success
			url = data.url
			PagSeguroLightbox
				code: data.code
			,
			success: (transactionCode) ->
				root.flash 'Compra efetuada com sucesso', 'success'
				location.href = data.return_url
			abort: ->
				root.flash 'A compra foi cancelada.'
		else
			root.flash data.message
