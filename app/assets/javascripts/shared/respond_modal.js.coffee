root = exports ? this

$ ->
	$('.modal-render-link').each ->
		return_modal($(this))

return_modal = (elem) ->
	elem.bind 'ajax:success', (evt, data, status, xhr) ->
		$this = $(this)
		show_modal data, $this, elem

show_modal = (data, elem, parent) ->
	if data['status']
		$('.modal-body').html data['html_elem']

		wid = elem.data('width') ? "700px"
		$('#modal').modal({ width: wid })
		$('#modal').find('.modal-render-link').each ->
			return_modal($(this))
	else
		$('.modal-body').html data['html_elem']
		# $('h1', '.modal-header').text 'Erro'

		wid = elem.data('width') ? "730px"
		$('#modal').modal { width: wid }

$ ->
	$('.modal-trigger').click ->
		$('#img-modal').find('.modal-body').html($(this).data('content'))
		$('#img-modal').modal({ width: '700px' })
