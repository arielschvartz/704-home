$ ->
	tinymce.init({
		selector:'#post_content',
		plugins: 'link hr anchor pagebreak wordcount code paste textcolor table contextmenu insertdatetime visualblocks fullscreen',
		toolbar1: "undo redo | styleselect | sizeselect | forecolor backcolor | bold italic | fontselect | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link"
	})

$ ->
	$('#post_media_type').change (event) ->
		$(this).parents('li:first').next().css 'display': 'none'
		$(this).parents('li:first').next().next().css 'display': 'none'
		if this.value == "Foto"
			$(this).parents('li:first').next().css 'display': 'block'
		else if this.value == "Vídeo"
			$(this).parents('li:first').next().next().css 'display': 'block'