window.photoUploaded = (event) ->
	if $('#color_product_photos', '.hidden').length > 0
		if $('#color_product_photos', '.hidden').val() == ''
			urls = []
		else
			urls = $('#color_product_photos', '.hidden').val().split(',')
	if $('#architect_project_photos', '.hidden').length > 0
		if $('#architect_project_photos', '.hidden').val() == ''
			urls = []
		else
			urls = $('#architect_project_photos', '.hidden').val().split(',')
	for key, value of event.fpfiles
		urls.push value.url
		addImage value.url
	$('#color_product_photos', '.hidden').val urls

addImage = (url) ->
	new_one = $('<div>').addClass('inline-element')
	old_photo = $('<div>').addClass('old_photo')
	img_holder = $('<div>').addClass('img_holder').append($('<img>').attr({
		src: url + '/convert?w=100&h=100&fit=max'
	}))
	overlay = $('<div>').addClass('over_img')
	icon = $('<span>').addClass('icon-remove')
	old_photo.append(img_holder)
	old_photo.append(overlay)
	old_photo.append(icon)
	new_one.append(old_photo)
	$('.inputs.photos').find('ol').append(new_one)
	addMouseEnter new_one
	addMouseLeave new_one
	addClickRemove icon

$ ->
	addMouseEnter $('.inline-element')
	addMouseLeave $('.inline-element')
	addClickRemove $('span.icon-remove')


addMouseEnter = (elem) ->
	elem.mouseenter (event) ->
		$(this).find('.over_img').stop(true, true).fadeTo('fast', 0.6)
		$(this).find('span').stop(true, true).fadeIn()

addMouseLeave = (elem) ->
	elem.mouseleave (event) ->
		$(this).find('.over_img').stop(true, true).fadeOut('fast')
		$(this).find('span').stop(true, true).fadeOut()

addClickRemove = (elem) ->
	elem.click (event) ->
		url = $(this).parent().find('img').prop('src').split('/convert')[0]
		if $('#color_product_photos', '.hidden').length > 0
			urls = $('#color_product_photos', '.hidden').val().split(',')
		if $('#architect_project_photos', '.hidden').length > 0
			urls = $('#architect_project_photos', '.hidden').val().split(',')
		urls = $.grep urls, (value) ->
			return value != url
		if $('#color_product_photos', '.hidden').length > 0
			$('#color_product_photos', '.hidden').val urls
		if $('#architect_project_photos', '.hidden').length > 0
			$('#architect_project_photos', '.hidden').val urls
		$(this).parent().parent().hide('fast')