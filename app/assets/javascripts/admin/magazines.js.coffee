set_upload_field = (elem) ->
	elem.after('<div class="progress"><div class="meter" style="width: 0%"></div></div>')
	id = elem.attr('id')
	$this = -> $("##{id}")
	$progress = elem.siblings('.progress').hide()
	$meter = $progress.find('.meter')
	elem.S3FileField
		add: (e, data) ->
			$progress.show()
			data.submit()
		done: (e, data) ->
			$progress.hide()
			addPage data.result.url
			# $this().attr(type: 'text', value: data.result.url, readonly: true)
		fail: (e, data) ->
			alert(data.failReason)
		progress: (e, data) ->
			progress = parseInt(data.loaded / data.total * 100, 10)
			console.log progress
			$meter.css(width: "#{progress}%")


model = undefined

$ ->
	$('.js-s3_file_field:last').each ->
		set_upload_field $(this)


window.pageUploaded = (event) ->
	urls = []
	for key, value of event.fpfiles
		urls.push value.url
		addPage value.url

addPage = (url) ->
	TOTAL = $('.page').length - 1

	page = $('#model').find('.page').clone()
	page.find('legend').find('span').text('Página '+ (TOTAL + 1))
	page.find('.page_photo').find('.left').find('img').prop 'src', url

	page.find('.left').find('img').next().prop {
		id: 'photo' + TOTAL,
		name: 'photo' + TOTAL,
		value: url
	}

	dragElem page
	$('#paginas').append(page)

$ ->
	$('.page').each (index, elem) ->
		dragElem $(this), index-1
	$('.added').each () ->
		dragAddedOne $(this)
	$('.page_photo').find('li').each () ->
		addHoverToSelect $(this)
	$('.remove_page').each () ->
		removePage $(this)

dragElem = (page, TOTAL = null) ->
	if TOTAL == null
		TOTAL = $('.page').length - 1

	page.find('.droppable_element').each () ->
		unless $(this).hasClass('added')
			$(this).draggable({
				helper: 'clone',
				# containment: page.find('img'),
				cursorAt: { left: 0, top: 0 }
			})

	page.find('.page_photo').droppable({
		activeClass: 'ui-state-default',
		hoverClass: 'ui-state-hover',
		accept: ':not(.ui-sortable-helper)',
		tolerance: 'pointer',
		drop: (event, ui) ->
			unless ui.draggable.hasClass('added')
				pos = ui.position

				new_posX = pos.left + ui.draggable.width()/2
				new_posY = pos.top + ui.draggable.height()

				img = $(this).find('img:first')
				if verify_bounds(img, new_posX, new_posY)
					new_element = ui.draggable.clone()
					new_element.css({
						position: 'absolute'
					}).offset(pos)
					new_element.addClass('added')
					$(this).find('.right').append(new_element)

					dragAddedOne new_element

					new_hidden(TOTAL, $(this), pos.left / img.width(), pos.top / img.height())
	})

dragAddedOne = (elem) ->
	elem.draggable({
		cursorAt: { left: 0, top: 0 }
		stop: (event, ui) ->
			parentOffset = $(this).parent().offset();
			pos = ui.position
			img = $(this).parents('.page_photo').find('img:first')

			pos.top = event.pageY - parentOffset.top - ui.helper.height() + $(this).height()
			# pos.left += $(this).width()/2

			$(this).next().val(pos.left / img.width())
			$(this).next().next().val(pos.top / img.height())

			unless verify_bounds(img, pos.left, pos.top)
				$(this).nextUntil('.droppable_element').remove()
				$(this).remove()
	})

verify_bounds = (img, left, top) ->
	if top > img.height() || top < 0 || left > img.width() || left < 0
		return false
	else
		return true

removePage = (elem) ->
	elem.click () ->
		$(this).parent().parent().remove()

new_hidden = (total, parent, posX, posY) ->
	HIDDEN_TOTAL = (parent.find('input[type="hidden"]').length - 1)/2

	for i in [0...HIDDEN_TOTAL]
		if parent.find('#posX' + total.toString() + i.toString()).length == 0
			HIDDEN_TOTAL = i
			break

	end_name = total.toString() + HIDDEN_TOTAL.toString()
	prod_select = parent.find('li:first').clone()
	prod_select.prop id: 'magazine_product_input' + end_name
	prod_select.find('label').prop for: 'magazine_product' + end_name
	prod_select.find('label').text('Produto ' + (HIDDEN_TOTAL + 1) + ' ')
	prod_select.find('select').prop id: 'magazine_product' + end_name
	prod_select.find('select').prop name: 'magazine[product' + end_name + ']'
	prod_select.css('display': 'block')

	website = parent.find('li:first').next().clone()
	website.prop id: 'magazine_website_input' + end_name
	website.find('label').prop for: 'magazine_website' + end_name
	website.find('label').text('Website ' + (HIDDEN_TOTAL + 1) + ' ')
	website.find('input').prop id: 'magazine_website' + end_name
	website.find('input').prop name: 'magazine[website' + end_name + ']'
	website.css 'display' : 'block'

	holder = parent.find('.right')
	holder.append(create_hidden_field('posX' + end_name, posX))
	holder.append(create_hidden_field('posY' + end_name, posY))
	holder.append(prod_select)
	holder.append(website)
	addHoverToSelect prod_select

addHoverToSelect = (elem) ->
	elem.mouseover (event) ->
		$(this).prevAll('.droppable_element:first').find('.hover').css 'visibility': 'visible'

	elem.mouseout (event) ->
		$(this).prevAll('.droppable_element:first').find('.hover').css 'visibility': 'hidden'

create_hidden_field = (my_name, my_value = '') ->
	return $('<input>').prop {
		type: 'hidden',
		id: my_name,
		name: my_name,
		value: my_value,
	}

$(window).load ->
	$('.droppable_element.added').each ->
		posX = parseFloat($(this).next().val())
		posY = parseFloat($(this).next().next().val())
		img = $(this).parents('.page_photo').find('img:first')
		posX = (img.width() * posX).toFixed(0)
		posY = (img.height() * posY).toFixed(0)
		$(this).css('left' : "#{posX}px")
		$(this).css('top' : "#{posY}px")
		console.log($(this).css('left'))
		console.log($(this).css('top'))
