$ ->	
	$('#share-fb').click ->
		window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog', 'menubar=no, toolbar=no, resizable=yes, scrollbar=yes, width=626, height=436')

	$('#share-twitter').click ->
		window.open('https://twitter.com/share?url=' + encodeURIComponent(location.href) + '&hashtags=704home', 'twitter-share-dialog', 'menubar=no, toolbar=no, resizable=yes, scrollbar=yes, width=626, height=436')

	$('#share-google').click ->
		window.open('https://plus.google.com/share?url=' + encodeURIComponent(location.href), 'google-share-dialog', 'menubar=no, toolbar=no, resizable=yes, scrollbar=yes, height=600, width=600')

	$('#share-pinterest').click ->
		window.open('http://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(location.href) + '&media=' + $('#container').find('img:first').prop('src'), 'pinterest-share-dialog', 'menubar=no, toolbar=no, resizable=yes, scrollbar=yes, height=300, width=600')