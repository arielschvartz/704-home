// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require shared/dictionary
//= require twitter/bootstrap/transition
//= require twitter/bootstrap/modal
//= require twitter/bootstrap/dropdown
//= require twitter/bootstrap/collapse
//= require twitter/bootstrap/tooltip
//= require twitter/bootstrap/carousel
//= require vendor/bootstrap-modal
//= require vendor/bootstrap-modalmanager
//= require shared/global_functions_and_actions
//= require shared/respond_modal
//= require header/menuslider
//= require header/header
//= require header/facebook_connect
//= require header/color_select
//= require spin
//= require product
//= require share
//= require calendar
//= require shared/cart
//= require magazine
//= require remove_alt_from_images
//= require comments
//= require home
//= require about
