opts =
	lines: 20
	length: 20
	width: 10
	radius: 100
	corners: 1
	rotate: 0
	direction: 1
	color: '#fff'
	speed: 1
	trail: 80
	shadow: true
	hwaccel: false
	className: 'spinner'
	zIndex: 2e9

spinner = new Spinner(opts).spin()

TOTAL_PAGES = undefined
ACTUAL_PAGE = 0

$ ->
	TOTAL_PAGES = $('.page_thumb').length
	$('.page_thumb').each (index, el) ->
		$(this).click ->
			goToPage index
	$('.arrow-right').click ->
		if ACTUAL_PAGE < TOTAL_PAGES - 1
			goToPage (ACTUAL_PAGE + 1)
	$('.arrow-left').click ->
		if ACTUAL_PAGE > 0
			goToPage (ACTUAL_PAGE - 1)

$(window).load ->
	$('img').each ->
		center_image $(this)

center_image = (elem) ->
	img = elem
	parent = img.parent()
	posX = (parent.width() - img.width())/2
	img.css 'left' : "#{posX}px"

goToPage = (i) ->
	if i == ACTUAL_PAGE
		return
	else
		if i < ACTUAL_PAGE
			left = -860
		else if i > ACTUAL_PAGE
			left = 860

		current_page = $('.page_container').find('.current')
		next_page = $('.page_container').find('.magazine_page_unit').eq(i)

		next_page.css left: (left + 'px')

		next_page.animate
			left: 0
		, 'fast'

		current_page.animate
			left: (-left + 'px')
		, 'fast', ->
			current_page.removeClass('current')
			next_page.addClass('current')

		ACTUAL_PAGE = i