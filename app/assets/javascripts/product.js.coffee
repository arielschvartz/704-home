$ ->
	$('.product_thumb').mouseover ->
		$(this).find('.overlay').find('.over').stop().fadeTo('fast', 1)
	$('.product_thumb').mouseout ->
		$(this).find('.overlay').find('.over').stop().fadeOut('fast')

	# $('html').click ->
	# 	$('.options').slideUp('fast')

	$('.my_select').each ->
		$(this).find('.custom_select').click (event) ->
			event.stopPropagation()
			if $(this).parent().find('.options').css('display') == 'none'
				$(this).parent().find('.options').stop().slideDown('fast')
			else
				$(this).parent().find('.options').stop().slideUp('fast')
		$(this).find('.selected').click (event) ->
			event.stopPropagation()
			if $(this).parent().find('.options').css('display') == 'none'
				$(this).parent().find('.options').stop().slideDown('fast')
			else
				$(this).parent().find('.options').stop().slideUp('fast')


	$('.color_select').find('a').click (event) ->
		event.stopPropagation()
		$(this).parent().parent().find('.selected').css 'background-color': $(this).css('background-color')
		$(this).parent().parent().find('.options').slideUp('fast')

	$('.ammount_select').find('.option').click (event) ->
		event.stopPropagation()
		$(this).parent().parent().find('.selected').html($(this).html())
		$(this).parent().parent().find('.options').slideUp('fast')

	$('.big_photo').mouseover (event) ->
		$(this).find('.hover').css
			visibility : 'visible'
		$('.zoom_photo').css
			visibility : 'visible'

	$('.big_photo').mouseout (event) ->
		$(this).find('.hover').css
			visibility : 'hidden'
		$('.zoom_photo').css
			visibility : 'hidden'

	$('.big_photo').mousemove (event) ->
		x = event.pageX - $(this).offset().left
		y = event.pageY - $(this).offset().top

		big = $('.big_photo')
		zoom = $('.zoom_photo')
		img = $('.zoom_photo').find('img')

		area_width = big.width() / (img.width()/zoom.width())
		area_height = big.height() / (img.height()/zoom.height())

		posX = x - (area_width/2)
		if posX < 0
			posX = 0
		else if posX > big.width() - (area_width)
			posX = big.width() - (area_width)

		posY = y - (area_height/2)
		if posY < 0
			posY = 0
		else if posY > big.height() - (area_height)
			posY = big.height() - (area_height)

		$('.hover').css
			width : "#{area_width}px"
			height : "#{area_height}px"
			left : "#{posX}px"
			top : "#{posY}px"

		imgY = posY * (img.height() / zoom.height())

		imgX = (posX / (big.width() - area_width)) * (img.width() - zoom.width())

		img.css
			left: "#{-imgX}px"
			top: "#{-imgY}px"

	$('.small_photos').find('img').click ->
		big_url = $(this).next().val()
		zoom_url = $(this).next().next().val()
		$('.zoom_photo').find('img').prop 'src' : zoom_url
		$('.big_photo').find('img:first').prop 'src' : big_url
		load_init()
		reset_loading_images()

	# reset_loading_images()

	$('.big_photo').find('img:first').load ->
		loaded++
		if loaded == 2
			load_finish()
	$('.zoom_photo').find('img').load ->
		loaded++
		if loaded == 2
			load_finish()

opts =
	lines: 20
	length: 20
	width: 10
	radius: 100
	corners: 1
	rotate: 0
	direction: 1
	color: '#fff'
	speed: 1
	trail: 80
	shadow: true
	hwaccel: false
	className: 'spinner'
	zIndex: 2e9

spinner = new Spinner(opts).spin()

loaded = 0

reset_loading_images = ->
	loaded = 0
	load_init()

load_init = ->
	spinner.spin()
	$('.big_photo').append(spinner.el)
	$('.spinner').css
		position : 'absolute'
		left : '50%'
		top : '50%'

load_finish = ->
	spinner.stop()
