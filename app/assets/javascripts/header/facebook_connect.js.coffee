root = exports ? this

window.fbAsyncInit = ->
	# init the FB JS SDK
	FB.init
		appId: "515722588509614" # App ID from the app dashboard
		channelUrl: "//localhost:3000/channel.html" # Channel file for x-domain comms
		status: true # Check Facebook Login status
		cookie: true # Enable Cookies
		xfbml: true # Look for social plugins on the page

	# Load the SDK asynchronously

((d, s, id) ->
	js = undefined
	fjs = d.getElementsByTagName(s)[0]
	return if d.getElementById(id)
	js = d.createElement(s)
	js.id = id
	js.src = "//connect.facebook.net/en_US/all.js"
	fjs.parentNode.insertBefore js, fjs
) document, "script", "facebook-jssdk"

root.set_facebook_buttons = () ->
	$('.facebook-login').not('.facebook-click-added').click (e) ->
		$(this).addClass('facebook-click-added')
		e.preventDefault()
		link = $(this).data('link')

		FB.login(
			(response) ->
				if response.authResponse
					window.location.replace(link)
				else
					flash_on_register_modal_facebook_errors()
			scope: 'email, offline_access, user_birthday, user_location, user_relationships, user_relationship_details'
		)	

flash_on_register_modal_facebook_errors = () ->
	flash = () ->
		html_elem = $('.modal-body').find('#new_user:first')
		root.clear_flash()
		root.flash "Autorização do Facebook negada.", 'error', html_elem
		if $('body').children('.modal-scrollable').length == 0
			$('.modal-render-link', '.secondary-login-links').unbind 'ajax:success', flash

	if $('body').children('.modal-scrollable').length == 0
		$('.modal-render-link', '.secondary-login-links').click()
		$('.modal-render-link', '.secondary-login-links').bind 'ajax:success', flash
	else
		flash()
	
