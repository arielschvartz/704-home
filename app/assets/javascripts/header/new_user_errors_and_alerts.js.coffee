root = exports ? this

$ ->
	$('form#new_user').bind 'ajax:beforeSend', () ->
		root.clear_flash()
	$('form#new_user').bind 'ajax:success', (evt, data, status, xhr) ->
		if data.status
			root.redirect data.location
		else
			for key, value of data.errors
				$(this).find('#' + 'user' + '_' + key).addClass 'error_field'
				root.flash "#{root.translate key} #{value.toString().split(',')[0]}", 'error', $(this)

$ ->
  $('form#edit_user').bind 'ajax:beforeSend', () ->
    root.clear_flash()
  $('form#edit_user').bind 'ajax:success', (evt, data, status, xhr) ->
    console.log(data)
    if data.status
      root.redirect data.location
    else
      for key, value of data.errors
        $(this).find('#' + 'user' + '_' + key).addClass 'error_field'
        root.flash "#{root.translate key} #{value.toString().split(',')[0]}", 'error', $(this)
