root = exports ? this

$ ->
	$('form#new_newsletter').bind 'ajax:success', (evt, data, status, xhr) ->
		if data.status
			root.flash "#{data.newsletter.email} cadastrado com sucesso.", 'success'
		else
			for key, value of data.errors
				root.flash "#{root.translate key} #{value.toString().split(',')[0]}", 'error'

	$('form[action="/login"]').bind 'ajax:success', (evt, data, status, xhr) ->
		if data.status
			root.redirect data['location']
		else
			root.clear_flash()
			root.flash_on_register_modal(data.errors, data.params)

	$('.search_area').find('.color-search').tooltip
		title: 'ESCOLHA UMA COR'
