$ ->
	# Menu Mouse Over
	$('ul.menu-slider').children('li').mouseover (event) ->
		slide($(this))

	# Menu Mouse Out
	$('ul.menu-slider').children('li').mouseout (event) ->
		if get_default_value($(this)) == -1
			slide_out($(this))
		else
			slide($('ul.menu-slider').children('li').eq(get_default_value($(this))))

	possibilites = window.location.toString().split('/')

	if window.location.toString().split('/')[3] == 'produtos'
		text = $('.title:first').text().split(' / ')[0]
		if $.trim(text) == "PRODUTOS"
			possibilites.push 'categorias'
		else
			possibilites.push 'ambientes'
	else if window.location.toString().split('/')[3] == 'posts'
		text = $('.title:first').text()
		possibilites.push 'post_categories'
		if $.trim(text) == "DIY"
			possibilites.push 'diy'
		else if $.trim(text) == "INSPIRE-SE"
			possibilites.push 'inspire-se'

	# Slide to current
	$('ul.menu-slider').children('li').each (index, el) ->
		elem = $(this)
		for i in [0...possibilites.length]
			if elem.find('a').attr('href').split('/')[1] == possibilites[i]
				if possibilites[i] == 'post_categories'
					if elem.find('a').attr('href').split('/')[2] == possibilites[i + 1]
						default_slide = index
						slide(elem, 0)
						set_default_value(elem, index)
				else
					default_slide = index
					slide(elem, 0)
					set_default_value(elem, index)




slide = (elem, time = 200) ->
	slider = elem.parent().parent().find('.slider')
	slider.stop().animate {
		width: elem.width(),
		left: elem.position().left
	}, time, ->
		#Animation complete

slide_out = (elem, time = 200) ->
	slider = elem.parent().parent().find('.slider')
	slider.stop().animate {
		left: '960px'
	}, time, ->
		# Animation complete

get_default_value = (elem) ->
	return parseInt elem.parent().find('input[type="hidden"]').val()

set_default_value = (elem, n) ->
	elem.parent().find('input[type="hidden"]').val(n)
