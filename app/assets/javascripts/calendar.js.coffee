MAX_MONTH = undefined
MAX_YEAR = undefined
MIN_MONTH = undefined
MIN_YEAR = undefined

$ ->
	MAX_MONTH = $('#max_month').data('month')
	MAX_YEAR = $('#max_year').data('year')
	MIN_YEAR = $('#min_year').data('year')
	MIN_MONTH = $('#min_month').data('month')
	year = parseInt($('table').data('year'))
	month = parseInt($('table').data('month'))
	if year == MIN_YEAR and month == MIN_MONTH
		$('.prev').css 'display' : 'none'
	if year == MAX_YEAR and month == MAX_MONTH
		$('.next').css 'display' : 'none'


	$('.next').bind 'ajax:success', (evt, data, status, xhr) ->
		create_table(data, 'left')
	$('.prev').bind 'ajax:success', (evt, data, status, xhr) ->
		create_table(data, 'right')



create_table = (data, dir) ->
	body = $('.calendar_body')
	old_table = body.find('table')
	new_table = $(data.content)
	body.append(new_table)
	animate_tables(old_table, new_table, dir)

animate_tables = (old_t, new_t, dir) ->

	if dir == 'left'
		old_pos = '100%'
		final_pos = '-100%'
	else
		old_pos = '-100%'
		final_pos = '100%'

	new_t.css
		left: old_pos
	old_t.animate
		left: final_pos
	, 'fast'
	new_t.animate
		left: '0'
	, 'fast', ->
		old_t.remove()
		changeLink('next', new_t)
		changeLink('prev', new_t)

changeLink = (name, new_t) ->
	link = $('.' + name)
	url = link.prop 'href'

	year = parseInt(new_t.data('year'))
	month = parseInt(new_t.data('month'))
	post = parseInt(url.split('/calendar/')[1])

	if name == 'next'
		new_month = month + 1

	else
		new_month = month - 1

	new_year = year

	if new_month > 12
		new_month = 1
		new_year = year + 1
	else if new_month < 1
		new_month = 12
		new_year = year - 1

	if new_month + 1 == MAX_MONTH and new_year == MAX_YEAR
		$('.next').css 'display' : 'none'
	else
		$('.next').css 'display' : 'block'
	if new_month + 1 == MIN_MONTH and new_year == MIN_YEAR
		$('.prev').css 'display' : 'none'
	else
		$('.prev').css 'display' : 'block'

	new_url = url.split('/year/')[0] + '/year/' + new_year + '/month/' + new_month + '/calendar/' + post

	link.prop 'href' : new_url



