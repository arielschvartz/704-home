xml.instruct!
xml.urlset(
  'xmlns'.to_sym => "http://www.sitemaps.org/schemas/sitemap/0.9",
  'xmlns:image'.to_sym => "http://www.google.com/schemas/sitemap-image/1.1"
) do
  @static_pages.each do |page|
    xml.url do
      xml.loc "#{page}"
      xml.changefreq("monthly")
    end
  end
  @posts_pages.each do |page|
    xml.url do
      xml.loc "#{page}"
      xml.changefreq("monthly")
    end
  end
  @color_products.each do |cp|
    xml.url do
      xml.loc "#{color_product_url(cp)}"
      xml.lastmod cp.updated_at.strftime("%F")
      xml.changefreq("monthly")
      if cp.photo.present?
        xml.image :image do
          xml.image :loc, "#{cp.photo.photo.url(:thumb)}"
        end
      end
    end
  end
  @posts.each do |post|
    xml.url do
      xml.loc "#{post_url(post)}"
      xml.lastmod post.updated_at.strftime("%F")
      xml.changefreq("monthly")
      if post.photo.present?
        xml.image :image do
          xml.image :loc, "#{post.photo.url(:thumb)}"
        end
      end
    end
  end
end