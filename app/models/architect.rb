class Architect < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, use: :slugged

	has_attached_file :photo, :styles => { :thumb => "140x175#", :medium => "156x156#" }
	
	attr_accessible :name, :description, :project_photos, :architect_photos_attributes, :photo

	has_many :posts, order: 'posts.date'
	has_many :architect_photos, dependent: :destroy
	accepts_nested_attributes_for :architect_photos, reject_if: lambda { |a| a[:photo].blank? }, allow_destroy: true

	serialize :project_photos

	validates_presence_of :name, :description

	before_validation :split_project_photos

	def photos
		architect_photos
	end

	private

		def split_project_photos
			if self.project_photos.class.name.to_s.downcase == 'string' and self.project_photos.split(',').length > 1
				self.project_photos = self.project_photos.split(',')
			end
		end
end
