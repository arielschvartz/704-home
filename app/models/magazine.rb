class Magazine < ActiveRecord::Base
	has_attached_file :pdf_file

	attr_accessible :pdf_file, :collection_id, :magazine_pages_attributes

	belongs_to :collection

	attr_accessor :photo, :product, :website

	has_many :magazine_pages, :dependent => :destroy
	accepts_nested_attributes_for :magazine_pages

	validates_presence_of :pdf_file
end
