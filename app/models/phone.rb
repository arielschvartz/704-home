class Phone < ActiveRecord::Base
  belongs_to :user
  attr_accessible :area_code, :number

  validates_presence_of :area_code, :number

  [:area_code, :number].each do |attr|
    define_method "#{attr}=".to_sym do |value|
      unless value.is_a? String
        value = value.to_s
      end

      value = value.gsub(/\D/, '')
      super(value)
    end
  end
end
