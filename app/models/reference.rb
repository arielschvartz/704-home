# encoding: UTF-8

class Reference < ActiveRecord::Base
	attr_accessible :pos_x, :pos_y, :product_id, :magazine_page_id, :website, :type_website

	belongs_to :magazine_page
	belongs_to :product

  def color_product
    ColorProduct.find(product_id)
  end

  def product?
    not(self.type_website)
  end

	validates_presence_of :magazine_page, :pos_x, :pos_y

  before_validation :set_type

  private

    def set_type
      if self.product_id.present?
        self.type_website = false
      elsif self.website.present?
        self.type_website = true
      else
        self.errors.add :product, "não pode ficar em branco se o website está em branco"
        self.errors.add :website, "não pode ficar em branco se o produto está em branco"
      end
      true
    end
end
