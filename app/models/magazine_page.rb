class MagazinePage < ActiveRecord::Base
	attr_accessible :photo_file_name

	belongs_to :magazine
	has_many :references, :dependent => :destroy

	validates_presence_of :photo_file_name

	def photo
		photo_file_name
	end
end
