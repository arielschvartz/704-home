# encoding: UTF-8

class Color < ActiveRecord::Base
	attr_accessible :code, :name, :local_id

	has_and_belongs_to_many :color_products

	validates_presence_of :code, :name
	validates_uniqueness_of :code, :name

	validates_uniqueness_of :local_id, allow_nil: true, allow_blank: true

	# def color_products
	# 	color_color_products.map { |x| x.color_product }
	# end
end
