class ExtraInfo < ActiveRecord::Base
  attr_accessible :club_704, :address, :business_name, :cnpj, :phone, :privacy_terms, :trade_terms, :about, :multibrand, :b2b, :delivery_time, :delivery_time_out_of_state
end
