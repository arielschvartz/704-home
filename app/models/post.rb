class Post < ActiveRecord::Base
	extend FriendlyId
	friendly_id :custom_slug, use: :slugged

	has_attached_file :photo, :styles => { :thumb => "311x311#", :medium => "650x515#" }

	process_in_background :photo

	attr_accessible :author, :date, :title, :post_category_id, :content, :media_link, :media_type, :architect_id, :slug, :photo

	belongs_to :post_category
	belongs_to :architect

	validates_presence_of :author, :date, :title, :content

	private

		def custom_slug
			if self.title.present?
				"#{self.title}"
			end
		end
end
