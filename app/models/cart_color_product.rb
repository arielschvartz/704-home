# encoding: UTF-8

class CartColorProduct < ActiveRecord::Base
	attr_accessible :ammount, :unit_price, :color_product_id

	belongs_to :cart
	belongs_to :color_product
	validates_associated :cart, :color_product

	validate :validate_product_aleardy_in_cart, on: :create

	validates_numericality_of :ammount, only_integer: true, greater_than: 0
	validates_numericality_of :unit_price, greater_than: 0

	def total_price
		ammount * unit_price
	end

	def to_pacote_item
		Correios::Frete::PacoteItem.new(
			peso: self.color_product.product.weight_in_grams / 1000,
			comprimento: self.color_product.product.depth_in_centimeters,
			largura: self.color_product.product.width_in_centimeters,
			altura: self.color_product.product.height_in_centimeters
		)
	end

	private

		def validate_product_aleardy_in_cart
			if CartColorProduct.where(cart_id: cart_id, color_product_id: color_product_id).limit(1).present?
				errors.add :cart_color_product, "já está no carrinho."
			end
		end
end
