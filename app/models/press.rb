class Press < ActiveRecord::Base
	has_attached_file :photo, :styles => { :thumb => "183x228#" }

  process_in_background :photo

  attr_accessible :photo

	validates_presence_of :photo
end
