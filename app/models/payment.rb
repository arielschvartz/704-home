# encoding: UTF-8

class Payment < ActiveRecord::Base
  attr_accessible :billing_address_city, :billing_address_complement, :billing_address_country, :billing_address_district, :billing_address_number, :billing_address_postal_code, :billing_address_state, :billing_address_street, :credit_card_holder_area_code, :credit_card_holder_birth_date, :credit_card_holder_cpf, :credit_card_holder_name, :credit_card_holder_phone, :credit_card_token, :currency, :extra_amount, :installment_quantity, :no_interest_installment_quantity, :installment_value, :notification_url, :payment_method, :payment_mode, :reference, :sender_area_code, :sender_cpf, :sender_email, :sender_hash, :sender_name, :sender_phone, :shipping_address_city, :shipping_address_complement, :shipping_address_country, :shipping_address_district, :shipping_address_number, :shipping_address_postal_code, :shipping_address_state, :shipping_address_street, :shipping_cost, :shipping_type, :trade_terms_and_conditions, :privacy_terms_and_conditions

  attr_accessible :card_number, :brand, :cvv, :expiration_month, :expiration_year, :billing_same_as_shipping

  serialize :response

  attr_accessor :card_number, :brand, :cvv, :expiration_month, :expiration_year, :billing_same_as_shipping, :trade_terms_and_conditions, :privacy_terms_and_conditions

  def billing_same_as_shipping= value
    if value == '1' or value == 'true' or value == true
      value = true
    else
      value = false
    end
    @billing_same_as_shipping = value
  end

  belongs_to :cart
  belongs_to :user

  has_many :payment_items

  # validates_presence_of :cart, :user
  validates_presence_of :currency, :payment_method, :payment_mode, :sender_email, :sender_name, :sender_cpf, :shipping_type, :shipping_cost, :shipping_address_street, :shipping_address_number, :shipping_address_country, :shipping_address_state, :shipping_address_city, :shipping_address_district, :shipping_address_postal_code

  # validates_presence_of :sender_area_code, :sender_phone, :sender_hash

  validates_presence_of :card_number, :cvv, :expiration_month, :expiration_year, if: Proc.new { |me| me.payment_method == 'creditCard' }

  validates_presence_of :credit_card_token, :credit_card_holder_name, :credit_card_holder_birth_date, :credit_card_holder_cpf, :credit_card_holder_area_code, :credit_card_holder_phone, if: Proc.new { |me| me.payment_method == 'creditCard' }

  validates_presence_of :billing_address_street, :billing_address_number, :billing_address_country, :billing_address_state, :billing_address_city, :billing_address_district, :billing_address_postal_code, if: Proc.new { |me| me.payment_method == 'creditCard' }

  validates_presence_of :installment_quantity, :installment_value, if: Proc.new { |me| me.payment_method == 'creditCard' }

  validates_inclusion_of :payment_method, in: ['creditCard', 'boleto']
  validates_inclusion_of :payment_mode, in: ['default']

  validates_acceptance_of :trade_terms_and_conditions, :privacy_terms_and_conditions

  before_validation :autocomplete_billing, if: Proc.new { |me| me.billing_same_as_shipping }
  before_validation :set_installment_info, if: Proc.new { |me| me.payment_method == 'creditCard' }

  after_save :update_user_info
  after_create :set_cart_payment_date

  def get_response
    if PAGSEGURO_SANDBOX
      url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/'
    else
      url = 'https://ws.pagseguro.uol.com.br/v2/transactions/'
    end

    response = HTTParty.post(url, body: self.to_hash)

    if response['errors'].present? or response['erro'].present?
      add_errors_from_pagseguro_codes response['errors']['error']
      if self.no_interest_installment_quantity.nil?
        self.no_interest_installment_quantity = self.installment_quantity
      end
      false
    else
      self.response = response.to_hash
      true
    end
  end

  def autocomplete_info_from_cart
    return unless self.cart.present?

    self.user = self.cart.user

    self.set_basic_info
    self.set_sender_info
    self.set_credit_card_holder_info
    self.set_shipping_info
    self.set_billing_info
    self.create_items self.cart.cart_color_products
  end

  def set_basic_info
    self.currency = 'BRL'
    self.payment_mode = 'default'
    self.reference = self.cart.id
    self.extra_amount = -self.cart.discount
    self.installment_quantity = 1
    self.no_interest_installment_quantity = 1
  end

  def set_sender_info
    self.sender_name = self.user.name
    if PAGSEGURO_SANDBOX
      self.sender_email = 'c09226587954587064045@sandbox.pagseguro.com.br'
    else
      self.sender_email = self.user.email
    end
    self.sender_cpf = self.user.cpf

    if self.user.phone.present?
      self.sender_area_code = self.user.phone.area_code
      self.sender_phone = self.user.phone.number
    end
  end

  def set_credit_card_holder_info
    self.credit_card_holder_name = self.user.name
    if self.user.birthday
      self.credit_card_holder_birth_date = self.user.birthday.strftime('%d/%m/%Y')
    end
    self.credit_card_holder_cpf = self.user.cpf

    if self.user.phone.present?
      self.credit_card_holder_area_code = self.user.phone.area_code
      self.credit_card_holder_phone = self.user.phone.number
    end
  end

  def set_shipping_info
    self.shipping_cost = self.cart.shipping
    self.shipping_type = 1

    if self.user.shipping_address.present?
      [:shipping_address_postal_code, :shipping_address_street, :shipping_address_number, :shipping_address_complement, :shipping_address_district, :shipping_address_city, :shipping_address_state].each do |attr|
        self.send "#{attr}=", self.user.shipping_address.send("#{attr.to_s.split('shipping_address_').last}".to_sym)
      end
    end
  end

  def set_billing_info
    if self.user.billing_address.present?
      [:billing_address_postal_code, :billing_address_street, :billing_address_number, :billing_address_complement, :billing_address_district, :billing_address_city, :billing_address_state].each do |attr|
        self.send "#{attr}=", self.user.billing_address.send("#{attr.to_s.split('billing_address_').last}".to_sym)
      end
    end
  end

  def create_items cart_items
    self.payment_items.each do |pi|
      pi.destroy
    end
    cart_items.each do |ci|
      pi = self.payment_items.new
      pi.payment = self
      pi.autocomplete_info_from_cart_item(ci)
    end
  end

  [:billing_address_postal_code, :shipping_address_postal_code, :sender_cpf, :credit_card_holder_cpf, :sender_phone, :credit_card_holder_phone, :sender_area_code, :credit_card_holder_area_code].each do |attr|
    define_method "#{attr}=".to_sym do |value|
      unless value.is_a? String
        value = value.to_s
      end

      value = value.gsub(/\D/, '')
      super(value)
    end
  end

  def autocomplete_billing
    [:billing_address_postal_code, :billing_address_street, :billing_address_number, :billing_address_complement, :billing_address_district, :billing_address_city, :billing_address_state].each do |attr|
      self.send("#{attr}=", self.send("#{attr.to_s.gsub('billing', 'shipping')}"))
    end
  end

  def set_installment_info
    if self.no_interest_installment_quantity == 1
      self.no_interest_installment_quantity = nil
      self.installment_quantity = 1
    else
      self.installment_quantity = self.no_interest_installment_quantity
    end
  end

  def update_user_info
    update_user_basic_info
    update_user_billing_address
    update_user_shipping_address
    true
  end

  def update_user_basic_info
    if self.user.name != self.sender_name
      self.user.name = self.sender_name
    end

    # if self.user.cpf != self.sender_cpf
    #   self.user.cpf = self.sender_cpf
    # end

    if self.credit_card_holder_birth_date.present?
      if self.sender_cpf == self.credit_card_holder_cpf
        self.user.birthday = Time.zone.parse(self.credit_card_holder_birth_date).to_date
      end
    end

    if self.sender_area_code.present? and self.sender_phone.present?
      phone = self.user.phone || Phone.new
      if phone.user.nil?
        phone.user = self.user
      end
      phone.area_code = self.sender_area_code
      phone.number = self.sender_phone
      phone.save
    end

    if self.user.valid?
      self.user.save
    end
  end

  def update_user_billing_address
    billing_address = self.user.billing_address || self.user.addresses.new(billing: true)

    [:billing_address_postal_code, :billing_address_street, :billing_address_number, :billing_address_complement, :billing_address_district, :billing_address_city, :billing_address_state].each do |attr|
      billing_address.send "#{attr.to_s.split('billing_address_').last}=", self.send(attr)
    end

    if billing_address.valid?
      billing_address.save
    end
  end

  def update_user_shipping_address
    shipping_address = self.user.shipping_address || self.user.addresses.new(shipping: true)

    [:shipping_address_postal_code, :shipping_address_street, :shipping_address_number, :shipping_address_complement, :shipping_address_district, :shipping_address_city, :shipping_address_state].each do |attr|
      shipping_address.send "#{attr.to_s.split('shipping_address_').last}=", self.send(attr)
    end

    if shipping_address.valid?
      shipping_address.save
    end
  end

  def set_cart_payment_date
    # self.cart.update_attribute(:payment_date, Time.zone.now)
  end

  def payment_link
    if self.response.present? and self.response['transaction'].present?
      self.response['transaction']['paymentLink']
    else
      nil
    end
  end

  def to_hash
    hash = {}
    [:billing_address_city, :billing_address_complement, :billing_address_country, :billing_address_district, :billing_address_number, :billing_address_postal_code, :billing_address_state, :billing_address_street, :credit_card_holder_area_code, :credit_card_holder_birth_date, :credit_card_holder_cpf, :credit_card_holder_name, :credit_card_holder_phone, :credit_card_token, :currency, :extra_amount, :installment_quantity, :no_interest_installment_quantity, :installment_value, :notification_url, :payment_method, :payment_mode, :reference, :sender_area_code, :sender_cpf, :sender_email, :sender_hash, :sender_name, :sender_phone, :shipping_address_city, :shipping_address_complement, :shipping_address_country, :shipping_address_district, :shipping_address_number, :shipping_address_postal_code, :shipping_address_state, :shipping_address_street, :shipping_cost, :shipping_type].each do |attr|
      next if self.send(attr).blank?
      if attr == :sender_cpf
        key = 'senderCPF'
      elsif attr == :credit_card_holder_cpf
        key = 'creditCardHolderCPF'
      else
        key = self.camelize(attr.to_s)
      end
      value = self.send(attr)

      if value.is_a? Float
        value = self.decimalize(value)
      end

      hash[key] = value
    end

    hash.merge!(email: PagSeguro.configuration.email, token: PagSeguro.configuration.token)

    self.payment_items.each_with_index do |pi, i|
      [:item_amount, :item_description, :item_id, :item_quantity].each do |attr|
        key = self.camelize(attr.to_s) + (i + 1).to_s
        value = pi.send(attr)

        if value.is_a? Float
          value = self.decimalize(value)
        end

        hash[key] = value
      end
    end

    hash
  end

  def camelize str
    str = str.split('_').map { |x| x[0].upcase + x[1..-1].downcase }.join('')
    str[0].downcase + str[1..-1]
  end

  def decimalize str
    unless str.is_a? String
      str = str.to_s
    end

    parts = str.split('.')
    parts[-1] = parts[-1].to_s.ljust(2, '0')

    parts.join('.')
  end

  def add_errors_from_pagseguro_codes errors
    if errors.is_a? Hash
      errors = [errors]
    end

    errors.each do |error_hash|
      k = error_hash['code']
      message = error_hash['message']
      error = pagseguro_error_codes[k]

      key = error.keys.first
      value = error.values.first

      if value.is_a? Hash
        sub_error = value
        self.errors.add "#{key}.#{sub_error.keys.first}", sub_error.values.first
      else
        self.errors.add key, value
      end
    end
  end


  def pagseguro_error_codes
    {
      '10000' => {
        brand: pagseguro_invalid_message
      },
      '10001' => {
        card_number: pagseguro_invalid_message
      },
      '10003' => {
        cvv: pagseguro_invalid_message
      },
      '10004' => {
        cvv: pagseguro_required_message
      },
      '10006' => {
        cvv: pagseguro_invalid_message
      },
      '53004' => {
        payment_items: {
          quantity: pagseguro_invalid_message
        }
      },
      '53005' => {
        currency: pagseguro_required_message
      },
      '53006' => {
        currency: pagseguro_invalid_message
      },
      '53007' => {
        reference: pagseguro_invalid_message
      },
      '53008' => {
        notification_url: pagseguro_invalid_message
      },
      '53009' => {
        notification_url: pagseguro_invalid_message
      },
      '53010' => {
        sender_email: pagseguro_required_message
      },
      '53011' => {
        sender_email: pagseguro_invalid_message
      },
      '53012' => {
        sender_email: pagseguro_invalid_message
      },
      '53013' => {
        sender_name: pagseguro_required_message
      },
      '53014' => {
        sender_name: pagseguro_invalid_message
      },
      '53015' => {
        sender_name: pagseguro_invalid_message
      },
      '53017' => {
        sender_cpf: pagseguro_invalid_message
      },
      '53018' => {
        sender_area_code: pagseguro_required_message
      },
      '53019' => {
        sender_area_code: pagseguro_invalid_message
      },
      '53020' => {
        sender_phone: pagseguro_required_message
      },
      '53021' => {
        sender_phone: pagseguro_invalid_message
      },
      '53022' => {
        shipping_address_postal_code: pagseguro_required_message
      },
      '53023' => {
        shipping_address_postal_code: pagseguro_invalid_message
      },
      '53024' => {
        shipping_address_street: pagseguro_required_message
      },
      '53025' => {
        shipping_address_street: pagseguro_invalid_message
      },
      '53026' => {
        shipping_address_number: pagseguro_required_message
      },
      '53027' => {
        shipping_address_number: pagseguro_invalid_message
      },
      '53028' => {
        shipping_address_complement: pagseguro_invalid_message
      },
      '53029' => {
        shipping_address_district: pagseguro_required_message
      },
      '53030' => {
        shipping_address_district: pagseguro_invalid_message
      },
      '53031' => {
        shipping_address_city: pagseguro_required_message
      },
      '53032' => {
        shipping_address_city: pagseguro_invalid_message
      },
      '53033' => {
        shipping_address_state: pagseguro_required_message
      },
      '53034' => {
        shipping_address_state: pagseguro_invalid_message
      },
      '53035' => {
        shipping_address_country: pagseguro_required_message
      },
      '53036' => {
        shipping_address_country: pagseguro_invalid_message
      },
      '53037' => {
        credit_card_token: pagseguro_required_message
      },
      '53038' => {
        installment_quantity: pagseguro_required_message
      },
      '53039' => {
        installment_quantity: pagseguro_invalid_message
      },
      '53040' => {
        installment_value: pagseguro_required_message
      },
      '53041' => {
        installment_value: pagseguro_invalid_message
      },
      '53042' => {
        credit_card_holder_name: pagseguro_required_message
      },
      '53043' => {
        credit_card_holder_name: pagseguro_invalid_message
      },
      '53044' => {
        credit_card_holder_name: pagseguro_invalid_message
      },
      '53045' => {
        credit_card_holder_cpf: pagseguro_required_message
      },
      '53046' => {
        credit_card_holder_cpf: pagseguro_invalid_message
      },
      '53047' => {
        credit_card_holder_birth_date: pagseguro_required_message
      },
      '53048' => {
        credit_card_holder_birth_date: pagseguro_invalid_message
      },
      '53049' => {
        credit_card_holder_area_code: pagseguro_required_message
      },
      '53050' => {
        credit_card_holder_area_code: pagseguro_invalid_message
      },
      '53051' => {
        credit_card_holder_phone: pagseguro_required_message
      },
      '53052' => {
        credit_card_holder_phone: pagseguro_invalid_message
      },
      '53053' => {
        billing_address_postal_code: pagseguro_required_message
      },
      '53054' => {
        billing_address_postal_code: pagseguro_invalid_message
      },
      '53055' => {
        billing_address_street: pagseguro_required_message
      },
      '53056' => {
        billing_address_street: pagseguro_invalid_message
      },
      '53057' => {
        billing_address_number: pagseguro_required_message
      },
      '53058' => {
        billing_address_number: pagseguro_invalid_message
      },
      '53059' => {
        billing_address_complement: pagseguro_invalid_message
      },
      '53060' => {
        billing_address_district: pagseguro_required_message
      },
      '53061' => {
        billing_address_district: pagseguro_invalid_message
      },
      '53062' => {
        billing_address_city: pagseguro_required_message
      },
      '53063' => {
        billing_address_city: pagseguro_invalid_message
      },
      '53064' => {
        billing_address_state: pagseguro_required_message
      },
      '53065' => {
        billing_address_state: pagseguro_invalid_message
      },
      '53066' => {
        billing_address_country: pagseguro_required_message
      },
      '53067' => {
        billing_address_country: pagseguro_invalid_message
      },
      '53068' => {
        receiver_email: pagseguro_invalid_message
      },
      '53069' => {
        receiver_email: pagseguro_invalid_message
      },
      '53070' => {
        payment_items: {
          item_id: pagseguro_required_message
        }
      },
      '53071' => {
        payment_items: {
          item_id: pagseguro_invalid_message
        }
      },
      '53072' => {
        payment_items: {
          item_description: pagseguro_required_message
        }
      },
      '53073' => {
        payment_items: {
          item_description: pagseguro_invalid_message
        }
      },
      '53074' => {
        payment_items: {
          item_quantity: pagseguro_required_message
        }
      },
      '53075' => {
        payment_items: {
          item_quantity: pagseguro_invalid_message
        }
      },
      '53076' => {
        payment_items: {
          item_quantity: pagseguro_invalid_message
        }
      },
      '53077' => {
        payment_items: {
          item_amount: pagseguro_required_message
        }
      },
      '53078' => {
        payment_items: {
          item_amount: pagseguro_invalid_message
        }
      },
      '53079' => {
        payment_items: {
          item_amount: pagseguro_invalid_message
        }
      },
      '53081' => {
        sender: 'é relacionado ao recebedor.'
      },
      '53084' => {
        receiver: pagseguro_invalid_message
      },
      '53085' => {
        payment_method: pagseguro_unavailable_message
      },
      '53086' => {
        cart_total_amount: pagseguro_invalid_message
      },
      '53087' => {
        credit_card_data: pagseguro_invalid_message
      },
      '53091' => {
        sender_hash: pagseguro_invalid_message
      },
      '53092' => {
        brand: pagseguro_unavailable_message
      },
      '53095' => {
        shipping_type: pagseguro_invalid_message
      },
      '53096' => {
        shipping_cost: pagseguro_invalid_message
      },
      '53097' => {
        shipping_cost: pagseguro_invalid_message
      },
      '53098' => {
        cart_total_value: pagseguro_invalid_message
      },
      '53099' => {
        extra_amount: pagseguro_invalid_message
      },
      '53101'=> {
        payment_mode: pagseguro_invalid_message
      },
      '53102' => {
        payment_method: pagseguro_invalid_message
      },
      '53104' => {
        shipping_address: pagseguro_required_message
      },
      '53105' => {
        sender_email: pagseguro_required_message
      },
      '53106' => {
        credit_card_holder_name: pagseguro_invalid_message
      },
      '53109' => {
        sender_email: pagseguro_required_message
      },
      '53110' => {
        eft_bank: pagseguro_required_message
      },
      '53148' => {
        no_interest_installment_quantity: pagseguro_invalid_message
      }
    }
  end

  def pagseguro_invalid_message
    'é inválido.'
  end

  def pagseguro_required_message
    'é obrigatório.'
  end

  def pagseguro_unavailable_message
    'não disponível.'
  end
end
