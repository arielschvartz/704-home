class Collection < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

	attr_accessible :end_date, :initial_date, :name

	validates_presence_of :name, :initial_date

  has_many :products
end
