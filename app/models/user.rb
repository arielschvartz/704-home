class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :token_authenticatable, :confirmable
	# :lockable, :timeoutable and :rememberable
	devise :database_authenticatable, :registerable, :omniauthable, :recoverable, :trackable, :validatable

	# Setup accessible (or protected) attributes for your model
	attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :gender, :birthday, :location, :relationship_status, :uid, :provider, :cpf
	# attr_accessible :title, :body

	validates_presence_of :name
	validates_presence_of :cpf, :unless => -> { self.provider.present? }, on: :create

	attr_accessor :newsletter, :gift

	has_many :carts
	has_many :addresses
	has_one :phone

	usar_como_cpf :cpf

	# def cpf= value
	# 	value = CPF.new(value)
	# 	super
	# end

	def billing_address
		self.addresses.billing.first
	end

	def shipping_address
		self.addresses.shipping.first
	end

	def active_cart
		open_carts = self.carts.where(payment_date: nil).includes(:payments).where(payments: { cart_id: nil }).limit(1)
		if open_carts.length > 0
			open_carts.last
		else
			self.carts.create
		end
	end

	before_validation :stripping_and_capitalizing

	def self.find_for_facebook_oauth auth
		unless user = User.find_by_uid(auth.uid)
			unless user = User.find_by_email(auth.info.email)
				user = User.new(uid: auth.uid,
								email: auth.info.email,
								name: auth.info.name,
								provider: 'facebook',
								password: Devise.friendly_token[0,20])
			else
				user.uid = auth.uid
				user.provider = 'facebook'
			end
			# unless user.confirmed?
			# 	user.skip_confirmation!
			# end
			user.save
		end
		user
	end

	private

		def stripping_and_capitalizing
			if  self.name.present?
				self.name = self.name.split(' ').map { |word| word.capitalize }.join(' ')

				[:name, :email].each do |var|
					self[var].strip!
				end

				self.email.downcase!
			end
		end

end
