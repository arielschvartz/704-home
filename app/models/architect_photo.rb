class ArchitectPhoto < ActiveRecord::Base
	has_attached_file :photo, :styles => { :thumb => "156x156#" }

	process_in_background :photo

	attr_accessible :architect_id, :photo

	belongs_to :architect
end
