class SubAmbient < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: :slugged

  attr_accessible :name, :ambient_id

	validates :name, presence: true, uniqueness: true

	belongs_to :ambient
	has_many :products
end