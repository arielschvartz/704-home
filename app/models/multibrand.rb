class Multibrand < ActiveRecord::Base
  attr_accessible :address, :brand_name, :business_name, :business_phone, :city, :cnpj, :contact_email, :contact_name, :contact_phone, :neghborhood, :state, :zip_code

  validates_presence_of :address, :brand_name, :business_name, :business_phone, :city, :cnpj, :contact_email, :contact_name, :contact_phone, :neghborhood, :state, :zip_code

  validates :contact_email, format: { with: %r{.+@.+\..+} }, allow_blank: true

  after_save :send_email

  private

    def send_email
      ContactMailer.new_multibrand(self).deliver
    end
end
