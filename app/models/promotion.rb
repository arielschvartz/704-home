# encoding: utf-8

class Promotion < ActiveRecord::Base
  attr_accessible :active, :end_date, :start_date, :uses, :coupon_attributes, :absolute_advantage_attributes

  validates_presence_of :start_date, :coupon, :absolute_advantage
  validate :end_date_later_than_start_date

  has_one :coupon, inverse_of: :promotion
  accepts_nested_attributes_for :coupon

  has_one :absolute_advantage, inverse_of: :promotion
  accepts_nested_attributes_for :absolute_advantage

  def uses
    super || 0
  end

  private

    def end_date_later_than_start_date
      if self.start_date.present? and self.end_date.present?
        self.errors.add :end_date, 'não pode ser anterior à data de ínicio' if self.end_date < self.start_date
      end
    end
end
