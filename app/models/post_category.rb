class PostCategory < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, use: :slugged

	attr_accessible :name

	validates :name, presence: true, uniqueness: true

	has_many :posts, order: 'posts.date'
end
