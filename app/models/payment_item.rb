# encoding: UTF-8

class PaymentItem < ActiveRecord::Base
  belongs_to :payment

  attr_accessible :item_amount, :item_description, :item_id, :item_quantity

  validates_presence_of :item_amount, :item_description, :item_id, :item_quantity

  # validates_uniqueness_of :item_id, scope: [:payment_id]

  def autocomplete_info_from_cart_item cart_item
    self.item_quantity = cart_item.ammount
    self.item_description = cart_item.color_product.humanized_name
    self.item_id = cart_item.color_product.id
    self.item_amount = cart_item.unit_price.to_f
  end
end
