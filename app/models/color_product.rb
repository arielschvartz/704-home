# encoding: UTF-8

class ColorProduct < ActiveRecord::Base
	acts_as_paranoid

	extend FriendlyId
	friendly_id :custom_slug, use: :slugged

	# default_scope order('created_at DESC')

	attr_accessible :stock_ammount, :product_id, :color_ids, :local_id, :color_product_photos_attributes

	acts_as_followable
	acts_as_follower

	has_many :color_product_photos, dependent: :destroy, :autosave => true, order: "position ASC"
	accepts_nested_attributes_for :color_product_photos, allow_destroy: true

	validates_presence_of :stock_ammount, :colors

	serialize :photos

	belongs_to :product

	has_and_belongs_to_many :colors
	validates_associated :product

	validate :validate_only_color_product, on: :create

	validates_numericality_of :stock_ammount, only_integer: true

	validates_uniqueness_of :local_id, allow_nil: true, allow_blank: true

	after_save :alert_if_stock_ammount_is_low

	def photos
		color_product_photos
	end

	def photo ambient = false
		my_photo = nil
		if ambient
			my_photo = self.photos.where(ambient_default: true).first
		else
			my_photo = self.photos.where(product_default: true).first
		end
		my_photo ||= self.photos.first
	end

	def name
		if self.product.present? and self.colors.present?
			"#{self.product.name} - #{self.colors.map { |x| x.name }.join('_')}"
		else
			""
		end
	end

	def humanized_name
		if self.product.present? and self.colors.present?
			"#{self.product.name} - #{self.colors.map { |x| x.name }.join(' / ')}"
		else
			""
		end
	end

	# def colors
	# 	color_color_products.map { |x| x.color }
	# end

	private

		def custom_slug
			if self.product.present? and self.colors.present?
				self.name
			end
		end

		def validate_only_color_product
			ColorProduct.where(product_id: product_id).each do |cp|
				if cp.colors.map { |x| x.id } == color_ids
					errors.add :product, "Essa cor já está cadastrada."
				end
			end
		end

		def alert_if_stock_ammount_is_low
			# if stock_ammount <= LOW_STOCK_CONFIG[:low_stock_ammount]
			# 	MyMailer.low_stock(self).deliver
			# end
		end
end
