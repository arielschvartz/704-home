class Ambient < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: :slugged

  attr_accessible :name

	validates :name, presence: true, uniqueness: true

	has_many :sub_ambients
end
