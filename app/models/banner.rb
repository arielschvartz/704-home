class Banner < ActiveRecord::Base
  default_scope order('position ASC')

  has_attached_file :image, styles: { carousel: '560x600#' }

  attr_accessible :active, :image, :banner_style, :url, :position

  validates_presence_of :image, :url
end
