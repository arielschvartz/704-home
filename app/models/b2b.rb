class B2b < ActiveRecord::Base
  attr_accessible :brand_name, :business_name, :business_phone, :cnpj, :contact_email, :contact_name, :contact_phone

  validates_presence_of :brand_name, :business_name, :business_phone, :cnpj, :contact_email, :contact_name, :contact_phone

  validates :contact_email, format: { with: %r{.+@.+\..+} }, allow_blank: true

  after_save :send_email

  private

    def send_email
      ContactMailer.new_b2b(self).deliver
    end
end
