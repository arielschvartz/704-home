class ColorProductPhoto < ActiveRecord::Base
	has_attached_file :photo, :styles => { :home_thumb => "143x143#", :thumb => "230x230#", :show_thumb => "150x150>", :medium => "650x515#", :big => "1300x1030#" }

	process_in_background :photo

	attr_accessible :color_product_id, :photo, :ambient_default, :product_default, :position

	belongs_to :color_product

  before_validation :change_string_to_boolean_defaults

  private

    def change_string_to_boolean_defaults
      if self.ambient_default.present?
        if self.ambient_default == "1"
          self.ambient_default = true
        elsif self.ambient_default == "0"
          self.ambient_default = false
        end
      end

      if self.product_default.present?
        if self.product_default == "1"
          self.product_default = true
        elsif self.product_default == "0"
          self.product_default = false
        end
      end
    end
end
