# encoding: UTF-8

class Cart < ActiveRecord::Base
	attr_accessible :payment_date

	belongs_to :user
	has_many :cart_color_products
	has_many :payment_notifications

	validate :validate_user_has_no_open_carts, on: :create

  belongs_to :promotion

  has_many :payments

  default_scope { includes(:cart_color_products) }

  def full_total_price
    self.total_price.to_f + self.shipping
  end

	def total_price
		self.total_price_no_discount - self.discount
	end

  def total_price_no_discount
    cart_color_products.to_a.sum(&:total_price)
  end

  def discount
    self.promotion.present? ? self.promotion.absolute_advantage.amount : 0
  end

  def shipping
    if self.shipping_cost
      self.shipping_cost / 100.0
    else
      self.final_payment ? self.final_payment.shipping_cost : 0
    end
  end

  def final_payment
    self.payments.last
  end

  def total_items
    self.cart_color_products.map { |x| x.ammount }.sum
  end

	def pagseguro_pay notification_url, redirect_url
		return nil if self.cart_color_products.empty?

    payment = PagSeguro::PaymentRequest.new
    payment.shipping = PagSeguro::Shipping.new
    payment.shipping.cost = self.shipping

		payment.reference = self.id
		payment.notification_url = notification_url
		payment.redirect_url = redirect_url
    payment.max_age = 1200

		self.cart_color_products.each do |ccp|
			payment.items << {
				id: ccp.color_product.id,
				description: ccp.color_product.humanized_name,
				amount: ccp.color_product.product.price.to_f,
				quantity: ccp.ammount
			}
		end

    payment.extra_amount = -self.discount

		return payment.register
	end

  def closed?
    (self.payments.any? or self.payment_date.present?) ? true : false
	end

	def merge cart
		if cart.class == Cart
      cp_ids = self.cart_color_products.map { |x| x.color_product_id }
      cart.cart_color_products.each do |ccp|
        if cp_ids.include? ccp.color_product_id
          sccp = self.cart_color_products.where(color_product_id: ccp.color_product_id).limit(1).first
          sccp.ammount = sccp.ammount + ccp.ammount
          sccp.save
        else
          self.cart_color_products.push(ccp)
          self.save
        end
      end
      cart.reload
      cart.destroy
    else
      raise "Carrinho deve ser da classe Cart"
    end
	end

  def self.cleanup
    where(['updated_at < ? AND user_id IS NULL', 12.hours.ago]).find_each(&:destroy)
  end

  def clean
    any_clean = false

    self.cart_color_products.each do |cpp|
      if cpp.color_product.nil? or cpp.color_product.stock_ammount < cpp.ammount
        cpp.destroy
        any_clean = true
      end
    end

    any_clean
  end

  def to_shipping_package
    package = Correios::Frete::Pacote.new

    self.cart_color_products.each do |ccp|
      ccp.ammount.times do
        package.adicionar_item ccp.to_pacote_item
      end
    end

    package
  end

  def get_shipping_price zip_code
    zip_code = BrZipCode.formatted(zip_code).gsub('.', '')

    s = Correios::Frete::Calculador.new cep_origem: '22451-090', cep_destino: zip_code, encomenda: self.to_shipping_package

    s.calcular(:pac)
  end

	private

		def validate_user_has_no_open_carts
			if self.user.present?
				if Cart.where(user_id: user_id, payment_date: nil).includes(:payments).where(payments: { cart_id: nil }).limit(1).present?
					errors.add :cart, "já está aberto."
				end
			end
		end

end
