class Faq < ActiveRecord::Base
	attr_accessible :answer, :question

  default_scope order('created_at ASC')

	validates_presence_of :question, :answer
end
