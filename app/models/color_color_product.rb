class ColorColorProduct < ActiveRecord::Base
	attr_accessible :color_id, :color_product_id

	belongs_to :color
	belongs_to :color_product
end
