class AbsoluteAdvantage < ActiveRecord::Base
  belongs_to :promotion

  attr_accessible :amount

  validates_presence_of :amount, :promotion
end
