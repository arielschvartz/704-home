class Address < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user

  scope :shipping, where(shipping: true)
  scope :billing, where(shipping: true)

  attr_accessible :billing, :city, :complement, :district, :number, :postal_code, :shipping, :state, :street

  validates_presence_of :postal_code, :street, :number, :district, :city, :state
end
