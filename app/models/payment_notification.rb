class PaymentNotification < ActiveRecord::Base
	attr_accessible :cart_id, :city, :code, :complement, :country, :gross, :neighborhood, :number, :parcels, :state, :status, :street, :user_cpf, :user_email, :user_name, :user_phone, :zip_code, :payment_method_code, :transaction_content, :payment_method, :shipping_method, :shipping_cost, :date, :type_of_transaction, :discount_ammount, :fee_ammount, :net_ammount, :escrow_end_date, :extra_ammount, :syncronized

	after_save :set_cart_as_paid

	belongs_to :cart

  serialize :transaction_content

  before_validation :set_sync_to_false

	private

		def set_cart_as_paid
			if self.status == "3"
				self.cart.update_attribute(:payment_date, DateTime.now)
        if self.cart.promotion.present?
          self.cart.promotion.increment!(:uses)
        end
			end
		end

    def set_sync_to_false
      if self.syncronized.nil?
        self.syncronized = false
      end
      true
    end
end
