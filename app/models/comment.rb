class Comment < ActiveRecord::Base
	attr_accessible :approved, :content, :email, :name, :url

	validates_presence_of :content, :email, :name, :url

	scope :aprovado, where(approved: true)
	scope :rejeitado, where(approved: false)
	scope :novo, where(approved: nil)
end
