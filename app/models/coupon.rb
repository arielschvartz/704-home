class Coupon < ActiveRecord::Base
  belongs_to :promotion
  attr_accessible :code, :maximum_uses

  validates_presence_of :promotion, :code

  def valid?
    if not self.promotion.active
      return false
    end

    if self.promotion.start_date > Date.today
      return false
    end

    if self.promotion.end_date.present? and self.promotion.end_date <= Date.today
      return false
    end

    if self.maximum_uses.present? and self.promotion.uses >= self.maximum_uses
      return false
    end

    true
  end
end
