class Product < ActiveRecord::Base
	acts_as_paranoid

	attr_accessible :description, :name, :price, :specification, :ambient_id, :product_category_id, :collection_id, :sub_ambient_id, :promotional_price, :parcels, :local_id, :on_promotion, :spotlight, :magazine_description, :weight_in_grams, :height_in_centimeters, :width_in_centimeters, :depth_in_centimeters

	# default_scope order('created_at ASC')

	belongs_to :sub_ambient
	belongs_to :collection
	belongs_to :product_category

	has_many :color_products, dependent: :destroy

	validates_presence_of :name, :price, :description

	validates_uniqueness_of :local_id, allow_nil: true, allow_blank: true

	# def self.search search
	# 	if search
	# 		results = []
	# 		search.split(' ').each do |word|
	# 			results.push(self.where('name LIKE ?', "%#{word}%") +
	# 			self.includes(:color_products).includes(color_products: :colors).joins(:color_products).joins(color_products: :colors).uniq.map {|x| x.color_products}.flatten.map {|x| x.colors}.flatten.uniq.select { |x| x.name.downcase.include? word.downcase })
	# 		end
	#     return results
	#   else
	#     find(:all)
	#   end
	# end

	def price
		if self.on_promotion
			unless self.promotional_price.blank? or self.promotional_price == 0
				return self.promotional_price
			end
		end
		read_attribute(:price)
	end

	def no_promotion_price
		read_attribute(:price)
	end

	def promotion?
		self.price != read_attribute(:price)
	end

	def self.color_search(id)
		color = Color.find id
		color.color_products.map { |x| x.product }.uniq
	end
end
