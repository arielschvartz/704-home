class AmbientsController < ApplicationController
  def index
    @ambients = Ambient.all
    # @products = Product.joins(color_products: :color_product_photos).uniq

    # @color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).uniq.where(product_id: @products).page(params[:page]).per(PAGINATION[:four])
    @base_color_products = ColorProduct.where(id: ColorProduct.joins(:color_product_photos).includes(:colors).uniq).joins(product: :collection).order('collections.created_at DESC')

    @color_products = Kaminari.paginate_array(@base_color_products).page(params[:page]).per(PAGINATION[:three])
  end

  def show
  	@ambients = Ambient.all
    @ambient = Ambient.find(params[:id])
    # @products = @ambient.sub_ambients.includes(:products).map { |x| x.products }.flatten

    # @color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).uniq.where(product_id: @products).page(params[:page]).per(PAGINATION[:three])

    @base_color_products = ColorProduct.where(id: ColorProduct.joins(:color_product_photos).includes(:colors).uniq.joins(product: :sub_ambient).where(sub_ambients: { ambient_id: @ambient.id })).joins(product: :collection).order('collections.created_at DESC')

    @color_products = Kaminari.paginate_array(@base_color_products).page(params[:page]).per(PAGINATION[:three])

    render 'index'
  end
end
