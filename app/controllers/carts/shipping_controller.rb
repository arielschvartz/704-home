# encoding: UTF-8

class Carts::ShippingController < ApplicationController
  respond_to :js

  def index
    zip_code = params[:cart][:shipping_zip_code]
    if BrZipCode.strip zip_code
      get_cart.get_shipping_prices(zip_code)

      render json: { shipping_methods: get_cart.shipping_options_hash.to_json.html_safe, zip_code: params[:cart][:zip_code] }
    else
      get_cart.shipping_options_hash = nil
      get_cart.shipping_zip_code = nil
      get_cart.save
      render json: { message: 'CEP inválido.' }, status: :unprocessable_entity
    end
  end

  def create
    zip_code = params[:cart][:shipping_zip_code]

    if BrZipCode.strip zip_code
      s = current_cart.get_shipping_price zip_code

      if s.erro == '0'
        current_cart.update_attribute(:shipping_cost, (s.valor * 100).to_i)
        current_cart.reload

        v = s.valor
        v = v.to_s.split('.')
        v[-1] = v[-1].ljust(2, '0')
        v = "R$ #{v.join(',')}"

        total = current_cart.full_total_price
        total = total.to_s.split('.')
        total[-1] = total[-1].ljust(2, '0')
        total = "R$ #{total.join(',')}"

        render json: {
          shipping: {
            value: v,
            days: s.prazo_entrega,
            name: 'PAC'
          },
          cart: {
            total: total
          }
        }
      else
        render json: {
          status: 'error',
          message: 'Houve algum erro no cálculo do frete com o CEP informado. Verifique se o CEP está correto.'
        }
      end
    end
    # method = get_cart.shipping_options_hash.select { |x| x.codigo == params[:cart][:shipping_method] }.first

    # if method
    #   get_cart.shipping_hash = method
    #   unless get_cart.save
    #     render json: { message: "Ocorreu algum erro. Tente novamente ou entre em contato em (21) 2523-1146." }, status: :unprocessable_entity
    #   else
    #     flash.now[:success] = "Forma de entrega escolhido com sucesso."
    #   end
    # else
    #   render json: { message: 'Método de entrega inválido.' }, status: :unprocessable_entity
    # end
  end
end
