class SitemapController < ApplicationController
   def index
     @static_pages = [root_url, about_url]
     @posts_pages = PostCategory.all.map { |x| post_category_url(x) }
     @color_products = ColorProduct.all
     @posts = Post.all
     respond_to do |format|
       format.xml
     end
   end
 end