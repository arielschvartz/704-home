# encoding: UTF-8

class Users::SessionsController < Devise::SessionsController
	def create
		resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
		sign_in_and_redirect(resource_name, resource)
	end

	def sign_in_and_redirect(resource_or_scope, resource=nil)
		scope = Devise::Mapping.find_scope!(resource_or_scope)
		resource ||= resource_or_scope
		sign_in(scope, resource) unless warden.user(scope) == resource

		if params[:after_login_path].present? and params[:after_login_method].present?
			return respond_json_model(resource, location: "#{after_sign_in_path_for(resource)}?after_login_path=#{params[:after_login_path]}&after_login_method=#{params[:after_login_method]}")
		else
			return respond_json_model(resource, location: after_sign_in_path_for(resource))
		end
	end

	def failure
		errors_hash = {}
		if params[:user][:email].blank?
			errors_hash[:email] = "não pode ficar em branco."
		elsif params[:user][:password].blank?
			errors_hash[:password] = "não pode ficar em branco."
		else
			errors_hash[:email_or_password] = "estão incorretos."
		end
		return render :json => {:success => false, :errors => errors_hash, errors_html: get_html_as_string('shared/_errors', errors: errors_hash), params: params[:user]}
	end
end
