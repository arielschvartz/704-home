class Users::PasswordsController < Devise::PasswordsController
  def new
    self.resource = resource_class.new
    if request.format == "text/javascript"
      respond_html_for_modal 'users/passwords/new'
    else
      respond_with self.resource
    end
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    root_path
  end
end
