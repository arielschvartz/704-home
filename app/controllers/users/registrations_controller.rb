class Users::RegistrationsController < Devise::RegistrationsController
	def new
		if params[:after_login_path].present?
			session[:redirection] = { return_url: params[:after_login_path] }
		end

		build_resource({})
		if request.format == "text/javascript"
			respond_html_for_modal 'users/registrations/new'
		else
			respond_with self.resource
		end
	end

	def create
		if params[:user].present?
			register_newsletter = params[:user].delete(:newsletter)
		end
		build_resource(sign_up_params)

		if resource.save
			if register_newsletter == "1"
				Newsletter.create(email: params[:user][:email])
			end
			if resource.active_for_authentication?
				set_flash_message :notice, :signed_up if is_navigational_format?
				sign_up(resource_name, resource)
				if params[:after_login_path].present? and params[:after_login_method].present?
					if request.format == "text/javascript"
						respond_json_model resource, location: "#{after_sign_up_path_for(resource)}?after_login_path=#{params[:after_login_path]}&after_login_method=#{params[:after_login_method]}"
					else
						respond_with resource, location: "#{after_sign_up_path_for(resource)}?after_login_path=#{params[:after_login_path]}&after_login_method=#{params[:after_login_method]}"
					end
				else
					if request.format == "text/javascript"
						respond_json_model resource, location: after_sign_up_path_for(resource)
					else
						respond_with resource, location: after_sign_up_path_for(resource)
					end
				end
			else
				set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
				expire_session_data_after_sign_in!
				if request.format == "text/javascript"
					respond_json_model resource, location: after_inactive_sign_up_path_for(resource)
				else
					respond_with resource, :location => after_inactive_sign_up_path_for(resource)
				end
			end
		else
			clean_up_passwords resource
			if request.format == "text/javascript"
				respond_json_model resource
			else
				respond_with resource
			end
		end
	end

	def edit
		# build_resource({})
		if params[:after_login_path].present?
			session[:redirection] = { return_url: params[:after_login_path] }
		end
		if request.format == "text/javascript"
			respond_html_for_modal 'users/registrations/edit'
		else
			render :edit
		end
	end

	def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    if resource.update_without_password(account_update_params)
      yield resource if block_given?
      sign_in resource_name, resource, :bypass => true
    if request.format == "text/javascript"
				respond_json_model resource, location: after_update_path_for(resource)
			else
	      respond_with resource, :location => after_update_path_for(resource)
			end
    else
      clean_up_passwords resource
    	if request.format == "text/javascript"
    		respond_json_model resource
    	else
	      respond_with resource
    	end
    end
  end

  protected

  	def after_update_path_for resource
			if session[:redirection].present?
				redirection = session[:redirection]
				session[:redirection] = nil
				if redirection[:params].present?
					redirection[:return_url] + (redirection[:params][:ammount].present? ? "?ammount=#{redirection[:params][:ammount]}" : "")
				else
					redirection[:return_url]
				end
			else
				super
			end
		end
end
