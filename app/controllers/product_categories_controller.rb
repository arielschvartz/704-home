class ProductCategoriesController < ApplicationController
  def index
    @product_categories = ProductCategory.includes(:products).all
    # @products = Product.joins(color_products: :color_product_photos).uniq

    # @color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).uniq.where(product_id: @products).page(params[:page]).per(PAGINATION[:three])

    # @products = Product.all
    @base_color_products = ColorProduct.where(id: ColorProduct.joins(:color_product_photos).includes(:colors).uniq).joins(product: :collection).order('collections.created_at DESC')

    @color_products = Kaminari.paginate_array(@base_color_products).page(params[:page]).per(PAGINATION[:three])
    # @color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).select('DISTINCT(color_products.id), color_products.slug, color_products.product_id, color_products.stock_ammount').page(params[:page]).per(PAGINATION[:three])
  end

  def show
  	@product_categories = ProductCategory.includes(:products).all
    @product_category = ProductCategory.includes(:products).find(params[:id])
    # @products = @product_category.products

    # @color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).uniq.where(product_id: @products).page(params[:page]).per(PAGINATION[:three])

    products = @product_category.products

    @base_color_products = ColorProduct.where(id: ColorProduct.where(id: ColorProduct.joins(:color_product_photos).includes(:colors).uniq).includes(:product).where(product_id: products.pluck(:id)).uniq).includes(product: :collection).order('collections.created_at DESC')

    params[:page] ||= '0'
    params[:page] = params[:page].to_i

    # @color_products.in_groups_of(PAGINATION[:three])[params]

    @color_products = Kaminari.paginate_array(@base_color_products).page(params[:page]).per(PAGINATION[:three])
    # @color_products = @base_color_products.page(params[:page]).per(PAGINATION[:three])

    # @color_products = ColorProduct.where(id: ColorProduct.joins(:color_product_photos).includes(:colors).uniq).joins(:product).where(products: { product_category_id: @product_category.id }).joins(product: :collection).order('collections.created_at DESC').page(params[:page]).per(PAGINATION[:three])
    render 'index'
  end
end
