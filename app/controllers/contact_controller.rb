class ContactController < ApplicationController
	def create
		@message = EmailMessage.new(params[:email_message])

		if @message.valid?
			ContactMailer.new_message(@message).deliver
      flash[:success] = "Mensagem enviada com sucesso."
      session[:contact_success] = true
			redirect_to root_path
    else
      flash[:error] = "Erro ao enviar a mensagem."
      redirect_to root_path
		end
	end
end
