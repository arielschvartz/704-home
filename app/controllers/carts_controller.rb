# encoding: UTF-8

class CartsController < ApplicationController
  def index
    @carts = current_user.carts.includes(:payments).where('payment_date IS NOT NULL OR payments.cart_id IS NOT NULL').order('carts.created_at DESC')
  end

	def show
		@cart = current_cart
    if @cart.cart_color_products.empty?
      flash[:error] = "O seu carrinho está vazio."
      redirect_to root_path
    end
	end
end
