class ColorProductsController < ApplicationController
	def show
		@color_product = ColorProduct.find params[:id]

		if params[:sub_ambient_id].nil?
			if params[:product_category_id].nil?
				@category = @color_product.product.product_category
			else
				@category = ProductCategory.find params[:product_category_id]
			end
		else
			@category = SubAmbient.find params[:sub_ambient_id]
		end
	end
end