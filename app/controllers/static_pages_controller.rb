# encoding: UTF-8

class StaticPagesController < ApplicationController
  def about
    @title = "Quem somos"
    @content = ExtraInfo.first.about
    # render 'static_pages/pure_text'
  end

  def privacy
    @title = "Segurança e Privacidade"
    @content = ExtraInfo.first.privacy_terms
    render 'static_pages/pure_text'
  end

  def promotion_rules
    @title = "Regulamento Promocional"
    render 'static_pages/pure_text'
  end

  def trade
    @title = "Política de Troca"
    @content = ExtraInfo.first.trade_terms
    render 'static_pages/pure_text'
  end

  def club
    @title = "Clube 704 Home"
    @content = ExtraInfo.first.club_704
    # @insta_photos = HTTParty.get('https://api.instagram.com/v1/users/543317433/media/recent?count=60&access_token=543317433.1fb234f.d943d2c4c6bf412287f3c3e9daa7bafe')['data']
    @insta_photos = HTTParty.get('https://api.instagram.com/v1/tags/clube704home/media/recent?access_token=543317433.1fb234f.d943d2c4c6bf412287f3c3e9daa7bafe')['data']
    render 'static_pages/pure_text'
  end

  def faq
    @faqs = Faq.all
  end

  def home
    @products = Product.where(spotlight: true).joins(color_products: :color_product_photos).uniq.sample(8)

    @magazine_products = @products.first(4).map { |x| x.color_products.includes(:product).includes(:colors).sample(1).first }
    @products = @products.last(4)

    # @magazine = Magazine.includes(:magazine_pages).includes(magazine_pages: :references).includes(magazine_pages: { references: :product }).last
    # @magazine_products = Reference.includes(:product).where(magazine_page_id: MagazinePage.where(magazine_id: @magazine.id)).joins(product: { color_products: :color_product_photos }).map { |x| x.product }.sample(4)
   #  @magazine_products = @magazine.magazine_pages.map { |x| x.references }.flatten.uniq.map { |x| x.color_product }.uniq.sample(4)

    @architect = Architect.all.reject { |x| x.posts.where("date < ?", DateTime.now).empty? }.map { |x| x.posts.last }.sort_by { |x| x.date }.last.architect
    @architect = Architect.joins(:posts).where("posts.date < ?", DateTime.now).order("posts.date ASC").last


    @color_products = @products.map { |x| x.color_products.includes(:product).includes(:colors).sample(1).first }

    if @products.length < 4
      Product.joins(color_products: :color_product_photos).all.uniq.sample(4 - @products.length).each do |prod|
        @products.push prod
      end
    end

  	@post_diy = PostCategory.find_by_name('DIY').posts.where("date < ?", DateTime.now).last
  	@post_inspirese = PostCategory.find_by_name('Inspire-se').posts.where("date < ?", DateTime.now).last

    # @banner = Banner.where(active: true).last
    @banners = Banner.where(active: true)

    if request.path == thanks_path
      @payment = Payment.find params[:p]
      if @payment
        @thanks = request.path == thanks_path
      end
    end
  end

  def building
    render 'static_pages/building', layout: false
  end
end
