# encoding: utf-8

class CouponsController < ApplicationController
  def create
    @coupon = Coupon.find_by_code params[:coupon][:code]

    if @coupon.valid? and current_cart.update_attribute(:promotion_id, @coupon.promotion.id)
      flash[:success] = "Cupom adicionado com sucesso."
    else
      flash[:error] = "Cupom inválido."
    end
    # if @coupon.present?
    #   @promotion = @coupon.promotion
    #   if @promotion.start_date >= Date.today
    #     if (@promotion.end_date.present? ? (Date.today < @promotion.end_date) : true)
    #       if (@coupon.maximum_uses.present? ? (@promotion.uses < @coupon.maximum_uses) : true)
    #       # if @coupon.maximum_uses.present? and @promotion.uses < @coupon.maximum_uses
    #         if current_cart.update_attribute(:promotion_id, @promotion.id)
    #           flash[:success] = "Cupom adicionado com sucesso."
    #         else
    #           flash[:error] = "Ocorreu algum erro ao adicionar o cupom."
    #         end
    #       else
    #         flash[:error] = "Esse cupom não é mais válido"
    #       end
    #     else
    #       flash[:error] = "Esse cupom não é mais válido."
    #     end
    #   else
    #     flash[:error] = "Esse cupom não é mais válido."
    #   end
    # else
    #   flash[:error] = "Código de cupom inválido."
    # end
    redirect_to current_cart
  end
end
