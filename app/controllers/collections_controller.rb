class CollectionsController < ApplicationController
  def show
    @collection = Collection.find params[:id]

    @products = @collection.products

    @color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).uniq.where(product_id: @products).page(params[:page]).per(PAGINATION[:four])
  end
end
