class ArchitectsController < ApplicationController
  def index
    @architects = Architect.all.sort_by { |x| x.name }.reject { |x| x.posts.empty? }
    @post = @architects.map { |x| x.posts.last }.sort_by { |x| x.date }.last
    @architect = @post.architect
    render 'posts/show'
  end

  def show
  	@architects = Architect.all.sort_by { |x| x.name }.reject { |x| x.posts.empty? }
  	@architect = Architect.find params[:id]
    @post = @architect.posts.last
    render 'posts/show'
  end
end
