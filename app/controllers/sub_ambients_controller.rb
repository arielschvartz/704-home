class SubAmbientsController < ApplicationController
  def show
  	@ambients = Ambient.all
    @ambient = Ambient.find(params[:ambient_id])
    @sub_ambient = SubAmbient.find params[:id]
    # @products = @sub_ambient.products

    # @color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).uniq.where(product_id: @products).page(params[:page]).per(PAGINATION[:three])

    if @sub_ambient.present?
      @products = @sub_ambient.products
    elsif @ambient.present?
      @products = @ambient.products
    else
      @products = Product.all
    end

    @color_products = ColorProduct.where(id: ColorProduct.joins(:color_product_photos)).includes(:product).where(product_id: @products.pluck(:id)).joins(product: :collection).order('collections.created_at DESC').page(params[:page]).per(PAGINATION[:three])

    # @color_products = ColorProduct.where(id: ColorProduct.joins(:color_product_photos).includes(:colors).uniq.joins(:product).where(products: { sub_ambient_id: @sub_ambient.id })).joins(:product).order('products.created_at DESC').page(params[:page]).per(PAGINATION[:three])

    render 'ambients/index'
  end
end
