class PaymentNotificationsController < ApplicationController
	skip_before_filter :verify_authenticity_token, only: :create

	def create
		transaction = PagSeguro::Transaction.find_by_notification_code(params[:notificationCode])

		puts " PagSeguro Transaction"
		puts transaction
		puts "\n\n\n\n\n"

		@pn = PaymentNotification.new

		@pn.code = transaction.code
		@pn.cart_id = transaction.reference
		@pn.status = transaction.status.to_s

		@pn.user_phone = "#{transaction.sender.phone.area_code}#{transaction.sender.phone.number}"
		@pn.user_name = transaction.sender.name
		@pn.user_email = transaction.sender.email
		@pn.user_cpf = Cart.find(transaction.reference).user.cpf.to_s

		# @pn.payment_method = transaction.payment_method
		@pn.payment_method_code = transaction.payment_method
		@pn.gross = transaction.gross_amount
		@pn.parcels = transaction.installments

		@pn.street = transaction.shipping.address.street
		@pn.number = transaction.shipping.address.number
		@pn.complement = transaction.shipping.address.complement
		@pn.neighborhood = transaction.shipping.address.district
		@pn.city = transaction.shipping.address.city
		@pn.state = transaction.shipping.address.state
		@pn.zip_code = transaction.shipping.address.postal_code
		@pn.country = transaction.shipping.address.country

		@pn.shipping_method = transaction.shipping.type_name
		@pn.shipping_cost = transaction.shipping.cost

		@pn.date = transaction.created_at
		@pn.type_of_transaction = transaction.type

		@pn.discount_ammount = transaction.discount_amount
		@pn.fee_ammount = transaction.fee_amount
		@pn.net_ammount = transaction.net_amount
		@pn.escrow_end_date = transaction.escrow_end_date
		@pn.extra_ammount = transaction.extra_amount

		@pn.transaction_content = transaction

		render nothing: true, status: 200
	end
end
