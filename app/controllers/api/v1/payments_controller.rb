class Api::V1::PaymentsController < ApplicationController
  def index
    if params[:all] == "true" or params[:all] == true
      @payments = PaymentNotification.all
    else
      @payments = PaymentNotification.where(syncronized: false)
    end

    payment_items = []
    @payments.each do |p|
      payment_hash = {}
      items = []
      p.cart.cart_color_products.each do |ccp|
        hash = {}
        hash[:local_id] = ccp.color_product.local_id
        hash[:id] = ccp.color_product.id
        hash[:price] = ccp.unit_price
        hash[:ammount] = ccp.ammount
        items.push(hash)
      end
      payment_hash[:items] = items
      payment_items.push payment_hash
    end

    respond_to do |format|
      format.json do
        render json: { success: true, payments: @payments, payments_items: payment_items }
      end
    end
  end

  def show
    @payment = PaymentNotification.where(id: params[:id]).first

    if @payment.nil?
      respond_to do |format|
        format.json do
          render json: { success: false, message: "payment not found" }
        end
      end
    else
      items = []
      @payment.cart.cart_color_products.each do |ccp|
        hash = {}
        hash[:local_id] = ccp.color_product.local_id
        hash[:id] = ccp.color_product.id
        hash[:price] = ccp.unit_price
        hash[:ammount] = ccp.ammount
        items.push(hash)
      end
      respond_to do |format|
        format.json do
          render json: { success: true, payment: @payment, items: items }
        end
      end
    end
  end

  def sync
    @payment = PaymentNotification.where(id: params[:id]).first
    if @payment.nil?
      respond_to do |format|
        format.json do
          render json: { success: false, message: "payment not found" }
        end
      end
    else
      if params[:shipping_code].present?
        @payment.cart.update_attribute(:shipping_code, params[:shipping_code])
      end
      @payment.update_attribute(:syncronized, true)
      respond_to do |format|
        format.json do
          render json: { success: true, payment: @payment }
        end
      end
    end
  end

  def no_shipping_code
    @payments = PaymentNotification.where(syncronized: true).where(cart_id: Cart.where(shipping_code: nil))

    payment_items = []
    @payments.each do |p|
      payment_hash = {}
      items = []
      p.cart.cart_color_products.each do |ccp|
        hash = {}
        hash[:local_id] = ccp.color_product.local_id
        hash[:id] = ccp.color_product.id
        hash[:price] = ccp.unit_price
        hash[:ammount] = ccp.ammount
        items.push(hash)
      end
      payment_hash[:items] = items
      payment_items.push payment_hash
    end

    respond_to do |format|
      format.json do
        render json: { success: true, payments: @payments, payments_items: payment_items }
      end
    end
  end

  def set_shipping_code
    @payment = PaymentNotification.where(id: params[:id]).first
    if @payment.nil?
      respond_to do |format|
        format.json do
          render json: { success: false, message: "payment not found" }
        end
      end
    else
      if params[:shipping_code].present?
        @payment.cart.update_attribute(:shipping_code, params[:shipping_code])
        respond_to do |format|
          format.json do
            render json: { success: true, payment: @payment }
          end
        end
      else
        respond_to do |format|
          format.json do
            render json: { success: false, message: "shipping_code cannot be null" }
          end
        end
      end
    end
  end
end