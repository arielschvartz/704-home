class Api::V1::ColorsController < ApplicationController
	def index
		@colors = Color.all
		@colors.each_with_index do |color, i|
			Color.column_names.each do |attr|
				if @colors[i][attr].nil?
					@colors[i][attr] = -1
				end
			end
		end
		respond_to do |format|
			format.json do
				render json: { success: true, colors: @colors }
			end
		end
	end

	def show
		@color = Color.find_by_local_id params[:id]

		if @color.nil?
			respond_to do |format|
				format.json do
					render json: { success: false }
				end
			end
		else
			respond_to do |format|
				format.json do
					render json: { success: true, color: @color }
				end
			end
		end
	end

	def create
		@color = Color.new
		@color.local_id = params[:local_id]
		@color.code = params[:code]
		@color.name = params[:name]
		
		if @color.save
			respond_to do |format|
				format.json do
					render json: { success: true, id: @color.id, parameters: params }
				end
			end
		else
			respond_to do |format|
				format.json do
					render json: { success: false, errors: @color.errors, parameters: params }
				end
			end
		end
	end

	def update
		@color = Color.find(params[:id])
		
		if @color.nil?
			respond_to do |format|
				format.json do
					render json: { success: false, errors: "Color not found.", parameters: params }
				end
			end
		else
			params.each do |k, v|
				if @color.attributes.include? k.to_s
					@color[k] = v
				end
			end
			if @color.save
				respond_to do |format|
					format.json do
						render json: { success: true, id: @color.id, parameters: params }
					end
				end
			else
				respond_to do |format|
					format.json do
						render json: { success: false, errors: @color.errors, parameters: params }
					end
				end
			end
		end
	end
end