class Api::V1::ProductsController < ApplicationController
	def index
		@products = Product.unscoped.all
		@products.each_with_index do |product, i|
			Product.column_names.each do |attr|
				if @products[i][attr].nil?
					@products[i][attr] = -1
				end
			end
		end
		respond_to do |format|
			format.json do
				render json: { success: true, products: @products }
			end
		end
	end

	def show
		@product = Product.unscoped.find_by_local_id params[:id]

		if @product.nil?
			respond_to do |format|
				format.json do
					render json: { success: false }
				end
			end
		else
			respond_to do |format|
				format.json do
					render json: { success: true, product: @product }
				end
			end
		end
	end

	def create
		@product = Product.new
		@product.name = params[:name]
		@product.description = params[:description]
		@product.specification = params[:specification]
		@product.price = params[:price]
		@product.promotional_price = params[:promotional_price]
		@product.on_promotion = params[:on_promotion] || false
		@product.parcels = params[:parcels] || 1
		@product.local_id = params[:local_id]

		if @product.save
			respond_to do |format|
				format.json do
					render json: { success: true, id: @product.id, parameters: params }
				end
			end
		else
			respond_to do |format|
				format.json do
					render json: { success: false, errors: @product.errors, parameters: params }
				end
			end
		end
	end

	def update
		@product = Product.unscoped.find(params[:id])

		if @product.nil?
			respond_to do |format|
				format.json do
					render json: { success: false, errors: "Product not found.", parameters: params }
				end
			end
		else
			params.each do |k, v|
				if @product.attributes.include? k.to_s
					if k.to_s != 'specification'
						@product[k] = v
					end
				end
			end
			if @product.save
				respond_to do |format|
					format.json do
						render json: { success: true, id: @product.id, parameters: params }
					end
				end
			else
				respond_to do |format|
					format.json do
						render json: { success: false, errors: @product.errors, parameters: params }
					end
				end
			end
		end
	end
end
