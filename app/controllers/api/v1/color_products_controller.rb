class Api::V1::ColorProductsController < ApplicationController
	def index
		@color_products = ColorProduct.unscoped.all
		@color_products.each_with_index do |color_product, i|
			ColorProduct.column_names.each do |attr|
				if @color_products[i][attr].nil?
					@color_products[i][attr] = -1
				end
			end
		end
		respond_to do |format|
			format.json do
				render json: { success: true, color_products: @color_products }
			end
		end
	end

	def show
		@color_product = ColorProduct.unscoped.find_by_local_id params[:id]

		if @color_product.nil?
			respond_to do |format|
				format.json do
					render json: { success: false }
				end
			end
		else
			respond_to do |format|
				format.json do
					render json: { success: true, color_product: @color_product }
				end
			end
		end
	end

	def create
		@product = Product.find_by_local_id(params[:product_local_id])

		if @product.nil?
			respond_to do |format|
				format.json do
					render json: { success: false, errors: "Product not found.", parameters: params }
				end
			end
		else
			@color_product = @product.color_products.new
			@color_product.local_id = params[:local_id]
			@color_product.stock_ammount = params[:stock_ammount]

			@colors = []
			params[:color_local_ids].each do |id|
				color = Color.find_by_local_id id
				if color.nil?
					respond_to do |format|
						format.json do
							render json: { success: false, errors: "Color with id #{id} does not exist.", parameters: params }
							return
						end
					end
				else
					@colors << color
				end
			end
			@color_product.colors = @colors
			if @color_product.save
				respond_to do |format|
					format.json do
						render json: { success: true, id: @color_product.id, parameters: params }
					end
				end
			else
				respond_to do |format|
					format.json do
						render json: { success: false, errors: @color_product.errors, parameters: params }
					end
				end
			end
		end
	end

	def update
		@color_product = ColorProduct.unscoped.find(params[:id])

		if @color_product.nil?
			respond_to do |format|
				format.json do
					render json: { success: false, errors: "Color_Product not found.", parameters: params }
				end
			end
		else
			@colors = []
			params[:color_local_ids].each do |id|
				color = Color.find_by_local_id id
				if color.nil?
					respond_to do |format|
						format.json do
							render json: { success: false, errors: "Color with id #{id} does not exist.", parameters: params }
							return
						end
					end
				else
					@colors << color
				end
			end
			@color_product.colors = @colors
			@color_product.stock_ammount = params[:stock_ammount]
			if @color_product.save
				# colors.each do |color|
				# 	color_color_product = ColorColorProduct.create color_id: color.id, color_product_id: @color_product.id
				# end
				respond_to do |format|
					format.json do
						render json: { success: true, id: @color_product.id, parameters: params }
					end
				end
			else
				respond_to do |format|
					format.json do
						render json: { success: false, errors: @color_product.errors, parameters: params }
					end
				end
			end
		end
	end
end
