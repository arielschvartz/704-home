# encoding: UTF-8

class MultibrandsController < ApplicationController
  def new
  	@multibrand = Multibrand.new
  end

  def create
  	@multibrand = Multibrand.new params[:multibrand]
  	if @multibrand.save
      flash[:success] = "Formulário enviado com sucesso."
  		redirect_to root_path
  	else
      flash[:error] = "Todos os campos são obrigatórios"
  		render :new
  	end
  end
end
