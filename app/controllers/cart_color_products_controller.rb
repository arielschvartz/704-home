# encoding: UTF-8

class CartColorProductsController < ApplicationController
	def index
		ammount = 1
		unless params[:ammount].nil?
			ammount = params[:ammount].to_i
		end
		if current_cart.nil?
			flash[:error] = "Ocorreu um erro."
			redirect_to root_path
		else
			if current_cart.cart_color_products.map { |x| x.color_product_id }.include? params[:color_product_id].to_i
				change_ammount(params[:color_product_id].to_i, ammount)
				redirect_to cart_path(current_cart)
			else
				@cart_color_product = current_cart.cart_color_products.new color_product_id: params[:color_product_id].to_i, ammount: ammount, unit_price: ColorProduct.find(params[:color_product_id].to_i).product.price
				if @cart_color_product.save
					# flash[:success] = "Produto adicionado ao carrinho com sucesso."
					redirect_to cart_user_path(current_user)
				else
					flash[:error] = "Ocorreu um erro."
					redirect_to root_path
				end
			end
		end
	end

	def create
		ammount = (params[:ammount] || 1).to_i
		if current_cart.nil?
			session[:redirection] = { return_url: request.path, method: request.method, params: params, times: 2 }
			render json: {status: false, errors: { login: "obrigatório para adicionar produtos ao carrinho." }}
		else
			if current_cart.cart_color_products.map { |x| x.color_product_id }.include? params[:color_product_id].to_i
				if change_ammount(params[:color_product_id].to_i, ammount)
					respond_to do |format|
						format.html do
							# flash[:success] = "Quantidade alterada com sucesso."
							redirect_to current_cart
						end
						format.js { render json: { status: true, redirect: false, link: cart_path(current_cart) } }
						format.json { render json: { status: true, redirect: false, link: cart_path(current_cart) } }
					end
				else
					respond_to do |format|
						format.html do
							flash[:error] = "Não existem mais produtos no estoque."
							redirect_to current_cart
						end
						format.js { render json: { status: false, redirect: true, link: cart_path(current_cart) } }
						format.json { render json: { status: false, redirect: true, link: cart_path(current_cart) } }
					end
				end
			else
				@cart_color_product = current_cart.cart_color_products.new color_product_id: params[:color_product_id].to_i, ammount: ammount, unit_price: ColorProduct.find(params[:color_product_id].to_i).product.price
				if @cart_color_product.save
					# flash[:success] = "Produto adicionado ao carrinho com sucesso."
					render json: { status: true, redirect: true, link: cart_path(current_cart) }
				else
					render json: { status: false }
				end
			end
		end
	end

	def update

	end

	def destroy
		@cart_color_product = CartColorProduct.find(params[:id].to_i)
		redirect = false
		if @cart_color_product.cart.cart_color_products.length == 1
			redirect = true
		end
		new_price = @cart_color_product.cart.total_price - @cart_color_product.unit_price * @cart_color_product.ammount
		new_price = new_price.round(2).to_s.split('.')
		new_price = new_price.first + ',' + new_price.last.ljust(2, '0')
		@cart_color_product.destroy
		respond_to do |format|
			format.html do
				flash[:success] = "Produto removido com sucesso"
				redirect_to current_cart
			end
			format.json { render json: { status: true, redirect: redirect, id: params[:id].to_i, link: root_path, price: new_price } }
			format.js { render json: { status: true, redirect: redirect, id: params[:id].to_i, link: root_path, price: new_price } }
		end
	end

	private

		def change_ammount color_product_id, ammount
			@prod = current_cart.cart_color_products.where(color_product_id: color_product_id).first
			new_ammount = @prod.ammount + ammount
			if new_ammount < @prod.color_product.stock_ammount
				@prod.update_attribute(:ammount, new_ammount)
			else
				false
			end
		end
end
