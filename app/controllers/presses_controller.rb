class PressesController < ApplicationController
  def index
    @presses = Press.all
  end

  def show
    @press = Press.find(params[:id])
    respond_html_for_modal 'presses/show'
  end
end
