class CalendarController < ApplicationController
	def show
		calendar = DateTime.new(params[:year_id].to_i, params[:month_id].to_i)
		post = Post.find params[:id]
		current = nil
		if post.date.year == calendar.year and post.date.month == calendar.month
			current = post.date.day
		end
		render json: { content: get_html_as_string('shared/_calendar', month: calendar.month, year: calendar.year, current_date: current, post_category_id: post.post_category.id) }
	end
end
