class ProductsController < ApplicationController
	def index
		@products = Product.all
	end

	def show
		if params[:ambiend_id].nil?
			@category = ProductCategory.find params[:product_category_id]
		else
			@category = Ambient.find params[:ambiend_id]
		end
		@product = Product.find params[:id]
	end

	def search
		if params[:search].blank?
			@products = Product.all
		else
			@products = []
			if Rails.env.devlopment?
				params[:search].split(' ').each do |word|
					@products << Product.where("name LIKE '%#{word}%'") + Product.includes(:color_products).includes(color_products: :colors).joins(:color_products).joins(color_products: :colors).uniq.map {|x| x.color_products}.flatten.uniq.map {|x| x.colors}.flatten.uniq.select { |x| x.name.downcase.include? word.downcase }.map { |x| x.color_products }.flatten.uniq.map { |x| x.product }.flatten.uniq
				end
			else
				params[:search].split(' ').each do |word|
					@products << Product.where("name ILIKE ?", "%#{word}%") + Product.includes(:color_products).includes(color_products: :colors).joins(:color_products).joins(color_products: :colors).uniq.map {|x| x.color_products}.flatten.uniq.map {|x| x.colors}.flatten.uniq.select { |x| x.name.downcase.include? word.downcase }.map { |x| x.color_products }.flatten.uniq.map { |x| x.product }.flatten.uniq
				end
			end

			@products = @products.flatten.uniq

			@color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).uniq.where(product_id: @products).page(params[:page]).per(PAGINATION[:four]).order('color_products.created_at ASC')
		end

		@title = "Busca por #{params[:search]} - #{@products.length} Resultados"
	end

	def color_search
		@color = Color.find(params[:color_id])
		@products = Product.color_search(params[:color_id])

		@color_products = ColorProduct.joins(:color_product_photos).includes(:product, :colors).uniq.where(product_id: @products).where(colors: {id: params[:color_id]}).page(params[:page]).per(PAGINATION[:four])

		@title = "Busca por #{@color.name} - #{@products.length} Resultados"
		render 'products/search'
	end
end
