class CommentsController < ApplicationController
	def create
		@comment = Comment.new params[:comment]
		if @comment.save
			respond_to do |format|
				format.json do
					render json: { success: true }
				end
			end
		else
			respond_to do |format|
				format.json do
					render json: { success: false }
				end
			end
		end
	end
end