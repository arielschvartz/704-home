# encoding: UTF-8

class B2bsController < ApplicationController
  def new
  	@b2b = B2b.new
  end

  def create
  	@b2b = B2b.new params[:b2b]
  	if @b2b.save
      flash[:success] = "Formulário enviado com sucesso."
  		redirect_to root_path
  	else
      flash[:error] = "Todos os campos são obrigatórios"
  		render :new
  	end
  end
end
