# encoding: UTF-8

class PaymentsController < ApplicationController
	def index
	end

	def new
		get_pagseguro_session
		if @pagseguro_session['session'].present?
			@payment = current_cart.payments.new
			@payment.autocomplete_info_from_cart
		else
			flash[:error] = "Ocorreu algum erro no sistema. Por favor tente novamente mais tarde."
			redirect_to current_cart
		end
	end

	def create
		@payment = current_cart.payments.new
		@payment.autocomplete_info_from_cart
		@payment.assign_attributes(params[:payment])
		@payment.notification_url = payment_notifications_url
		@payment.redirect_url = thanks_url(t: current_cart.id, r: current_cart.full_total_price.round(2).to_s.ljust(5, '0'))

		if @payment.valid? and @payment.get_response
			@payment.save
			redirect_to "#{@payment.redirect_url}&p=#{@payment.id}"
		else
			get_pagseguro_session
			if @pagseguro_session['session'].present?
				flash[:error] = "Ocorreu um erro."
				render :new
			else
				flash[:error] = "Ocorreu algum erro no sistema. Por favor tente novamente mais tarde."
				redirect_to current_cart
			end
		end
	end

	def old_create
		if current_user.cpf.present?
			if params[:gift].present?
				current_user.active_cart.update_attribute(:gift, true)
			else
				current_user.active_cart.update_attribute(:gift, false)
			end

			# if current_user.save
			# response = current_user.active_cart.pagseguro_pay('https://www.704home.com.br', 'https://www.704home.com.br')
			response = current_user.active_cart.pagseguro_pay(payment_notifications_url, thanks_url(t: current_user.active_cart.id, r: current_user.active_cart.total_price.to_f.round(2).to_s.ljust(5, '0')))

			if response.errors.any?
				# flash.now[:error] = response.errors.join("\n")
				respond_to do |format|
					format.json do
						render json: { success: false, message: response.errors.join("\n") }
					end
				end
			else
				respond_to do |format|
					format.json do
						render json: { success: true, code: response.code, url: response.url, return_url: thanks_url(t: current_user.active_cart.id, r: current_user.active_cart.total_price.to_f.round(2).to_s.ljust(5, '0')) }
					end
				end
			end
		else
			respond_to do |format|
				format.json do
					render json: { success: false, message: 'CPF é obrigatório.' }
				end
			end
		end
	end

	def get_pagseguro_session
		if PAGSEGURO_SANDBOX
			@pagseguro_session = HTTParty.post('https://ws.sandbox.pagseguro.uol.com.br/v2/sessions/', body: { email: PagSeguro.configuration.email, token: PagSeguro.configuration.token })
		else
			@pagseguro_session = HTTParty.post('https://ws.pagseguro.uol.com.br/v2/sessions/', body: { email: PagSeguro.configuration.email, token: PagSeguro.configuration.token })
		end
	end
end
