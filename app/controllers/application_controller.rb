class ApplicationController < ActionController::Base
	protect_from_forgery
	include ApplicationHelper

	helper :layout

	before_filter :set_page_comments, :set_colors

	# def pry
	# 	binding.pry
	# end

	def after_sign_in_path_for resource
		if session[:redirection].present?
			redirection = session[:redirection]
			session[:redirection] = nil
			if redirection[:params].present?
				redirection[:return_url] + (redirection[:params][:ammount].present? ? "?ammount=#{redirection[:params][:ammount]}" : "")
			else
				redirection[:return_url]
			end
		else
			super
		end
	end

	def get_html_as_string file, locals = {}
		render_to_string(file, layout: false, formats: [:html], locals: locals)
	end

	def respond_html_for_modal file, locals = {}
		render json: {
			status: true, html_elem: get_html_as_string(file, locals)
		}
	end

	def respond_json_model object, others = {}
		if object.errors.empty?
			hash = {
				status: true,
				object.class.name.downcase.to_sym => object
			}
			render json: hash.merge(others)
		else
			hash = {
				status: false,
				object.class.name.downcase.to_sym => object,
				errors: object.errors,
				errors_html: get_html_as_string('shared/_errors', errors: object.errors),
				object.class.name.downcase.to_sym => object
			}
			render json: hash.merge(others)
		end
	end

	def set_page_comments
		@current_comments = Comment.where(url: request.path).aprovado
	end

	def set_colors
		@colors = Color.joins(:color_products).uniq
	end
end
