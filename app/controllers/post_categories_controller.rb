class PostCategoriesController < ApplicationController
  def show
    @post_category = PostCategory.find(params[:id])
    redirect_to @post_category.posts.where("date < ?", DateTime.now).sort_by { |x| x.date }.last
  end
end
