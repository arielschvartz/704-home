class ContactMailer < ActionMailer::Base
	default from: "noreply@704home.com.br"
	default to: "contato704home@704home.com.br"
  # default to: "ari.shh@gmail.com"

	def new_message(message)
		@message = message
		mail(subject: "Contato - Site 704 Home")
	end

  def new_multibrand(multibrand)
    @multibrand = multibrand
    mail(subject: "Nova Multimarca - Site 704 Home")
  end

  def new_b2b(b2b)
    @b2b = b2b
    mail(subject: "B2B - Site 704 Home")
  end
end
