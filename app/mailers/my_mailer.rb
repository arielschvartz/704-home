class MyMailer < ActionMailer::Base
	default from: "\"Suporte 704home\" <noreply.704home@gmail.com>"

	def low_stock(color_product)
		@color_product = color_product
		mail(to: LOW_STOCK_ALERT_EMAIL, subject: "Estoque de #{color_product.product.name}-#{color_product.color.name} baixo!")
	end
end
