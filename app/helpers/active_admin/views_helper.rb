module ActiveAdmin::ViewsHelper
	def generate_links_for resources, url_helper_symbol, names = [] 
		method = Rails.application.routes.url_helpers.method(url_helper_symbol)
		html = ''
		names = resources.map { |x| x.name } if names.empty?
		resources.each_with_index do |resource, i|
			html += link_to(names[i], method.call(resource)).to_s + " "
		end
		html.html_safe
	end

	def create_model_page_for form, pg = nil, i = nil
		ac = ActionController::Base.new
		("<div id='model'>" + ac.render_to_string("shared/_page", locals: { f: form, page: pg, i: i}) + "</div>")
	end
end