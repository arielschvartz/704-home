module ApplicationHelper
  def current_cart
    cart = Cart.where(id: cookies[:cart]).first

    if cart and cart.closed?
      cart = nil
      cookies[:cart] = nil
    end

    if user_signed_in?
      if cart.nil? or cart.closed?
        cart = current_user.active_cart
      else
        unless cart.user_id == current_user.id
          if cart.user_id.nil?
            current_user.active_cart.merge cart
            cart = current_user.active_cart
          else
            cart = current_user.active_cart
          end
        end
      end
    else
      if cart.nil? or cart.user_id.present?
        cart = Cart.create
      end
    end

    cookies[:cart] = { value: cart.id, expires: 12.hours.from_now }

    if cart.promotion.present? and cart.promotion.coupon.present? and not cart.promotion.coupon.valid?
      cart.update_attribute(:promotion_id, nil)
    end

    cart.clean

    cart
  end
end
