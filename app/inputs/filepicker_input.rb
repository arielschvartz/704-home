class FilepickerInput
	include Formtastic::Inputs::Base

	def to_html
		full_options = options.merge input_html_options
		input_wrapping do
			label_html <<
			builder.filepicker_field(method, full_options)
		end
	end
end