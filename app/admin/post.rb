# encoding: UTF-8

ActiveAdmin.register Post do
	menu parent: "Posts"
	
	form partial: 'form'

	controller do
		def create
			if params[:post][:media_type] == "Foto"
				params[:post][:media_link] = params[:post][:photo_link]
			elsif params[:post][:media_type] == "Vídeo"
				params[:post][:media_link] = params[:post][:video_link]
			end
			params[:post].delete(:video_link)
			params[:post].delete(:photo_link)
			super
		end

		def update
			if params[:post][:media_type] == "Foto"
				params[:post][:media_link] = params[:post][:photo_link]
			elsif params[:post][:media_type] == "Vídeo"
				params[:post][:media_link] = params[:post][:video_link]
			end
			params[:post].delete(:video_link)
			params[:post].delete(:photo_link)
			super
		end
	end

	index do
		column :title
		column :author
		column :date
		column :post_category
		column :architect
		column :media_type
		column :media_link do |p|
			if p.media_type == "Foto"
				show_photos [p.media_link]
			elsif p.media_type == "Vídeo"
				show_video p.media_link
			end
		end
		default_actions
	end

	show do
		attributes_table do
			row :title
			row :author
			row :date
			row :post_category
			row :architect
			row :content do |p|
				p.content.html_safe
			end
			row :media_type
			row :media_link
		end
	end

	filter :post_category 
	filter :author
	filter :date
	filter :title
end

def show_photos photos, show_all = false
	unless photos.empty?
		render "shared/img", photos: photos, show_all: show_all
	end
end

def show_video video
	render 'shared/video', video: video
end

def include_tinymce
	"<script src='//tinymce.cachefly.net/4.0/tinymce.min.js'></script>".html_safe
end