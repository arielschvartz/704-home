ActiveAdmin.register Ambient do
	menu parent: "Produtos"
	
	show do
		attributes_table do
			row :name
			row :sub_ambients do |ambient|
				generate_links_for ambient.sub_ambients, :admin_sub_ambient_path
			end
		end
	end

	index do
		column :name
		column :sub_ambients do |ambient|
			generate_links_for ambient.sub_ambients, :admin_sub_ambient_path
		end
		default_actions
	end

	filter :name
end
