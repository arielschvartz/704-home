ActiveAdmin.register Faq do
	menu parent: "Extras"

	form partial: 'form'

	show do
		attributes_table do
			row :question
			row :answer
		end
	end

	index do
		column :question
		column :answer
		default_actions
	end

	config.filters = false
end
