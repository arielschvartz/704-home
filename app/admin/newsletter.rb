ActiveAdmin.register Newsletter do
  actions :all, :except => [:new, :edit, :destroy]

  index do
    column :email
    default_actions
  end
end
