ActiveAdmin.register ExtraInfo do
	menu parent: "Extras"

	form partial: 'form'

	controller do
		def index
			if ExtraInfo.all.length > 0
				redirect_to admin_extra_info_path(1)
			else
				redirect_to new_admin_extra_info_path
			end
		end
	end
end
