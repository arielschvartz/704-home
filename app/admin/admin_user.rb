ActiveAdmin.register AdminUser do
	index do
		column :email
		column :current_sign_in_at
		column :last_sign_in_at
		column :sign_in_count
		default_actions
	end

	filter :email
	
	form do |f|
		f.inputs "Detalhes do Admin" do
			f.input :email
			f.input :password
			f.input :password_confirmation
		end
		f.actions
	end

	show do
		attributes_table do
			row :id
			row :email
			row :sign_in_count
			row :current_sign_in_at
			row :last_sign_in_at
			row :current_sign_in_ip
			row :last_sign_in_ip
		end
	end
end
