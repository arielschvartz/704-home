ActiveAdmin.register Color do
	menu parent: "Produtos"

	show do
		attributes_table do
			row :name
			row :code
		end
	end

	index do
		column :name
		column :code
		default_actions
	end

	collection_action :index, method: :get do
		scope = Color.unscoped

		@search = scope.metasearch(clean_search_params(params[:q]))
		@collection = @search.relation.order('name ASC').page(params[:page])
	end

	filter :name
	filter :code
end
