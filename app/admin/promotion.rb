ActiveAdmin.register Promotion do

  form do |f|
    f.inputs 'Detalhes Gerais' do
      f.input :active
      f.input :start_date, as: :date_picker
      f.input :end_date, as: :date_picker
    end

    f.inputs 'Cupom', for: [:coupon, f.object.coupon || Coupon.new] do |coupon_form|
      coupon_form.input :code
      coupon_form.input :maximum_uses
    end

    f.inputs 'Vantagem', for: [:absolute_advantage, f.object.absolute_advantage || AbsoluteAdvantage.new] do |adv_form|
      adv_form.input :amount
    end

    f.actions
  end

  index do
    column :active
    column :start_date
    column :end_date
    column :coupon_code do |p|
      p.coupon.code
    end
    column :advantage do |p|
      number_to_currency p.absolute_advantage.amount
    end
    default_actions
  end

  show do |p|
    attributes_table do
      row :active
      row :start_date
      row :end_date
      row :uses
      row :maximum_uses do
        p.coupon.maximum_uses
      end
      row :coupon_code do
        p.coupon.code
      end
      row :advantage do
        number_to_currency p.absolute_advantage.amount
      end
    end
  end

end
