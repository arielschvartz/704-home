ActiveAdmin.register Collection do
  actions :index, :show, :new, :create, :edit, :update

	filter :name
	filter :initial_date
	filter :end_date

  index do
    column :name
    column :url do |c|
      collection_url(c)
    end
    default_actions
  end

  form do |f|
    f.inputs 'Detalhes' do
      f.input :name
      f.input :initial_date
      f.input :end_date
    end

    f.actions
  end
end
