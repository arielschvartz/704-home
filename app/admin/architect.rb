ActiveAdmin.register Architect do
	menu parent: "Posts"

	form do |f|
		f.inputs "Detalhes do Arquiteto" do
			f.input :name
			f.input :description
			f.input :photo
		end
		f.inputs "Fotos de Projetos" do
			f.has_many :architect_photos, allow_destroy: true do |af|
				af.input :photo
			end
		end
		f.actions
	end

	show do
		attributes_table do
			row :name
			row :description
			row :photo do |a|
				show_photos [a]
			end
			row :project_photos do |a|
				show_photos a.photos
			end
		end
	end

	index do
		column :name
		column :description
		column :photo do |a|
			show_photos [a]
		end
		column :project_photos do |a|
			show_photos a.photos
		end
		default_actions
	end

	filter :name
end


def show_photos photos, show_all = false
	unless photos.empty?
		render "shared/img", photos: photos, show_all: show_all
	end
end