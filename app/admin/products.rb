ActiveAdmin.register Product do
	menu parent: "Produtos"

	form partial: 'form'

	index do
		column :name
		column :description
		column :specification
		column :price do |prod|
			prod.no_promotion_price
		end
		column :promotional_price
		column :parcels
		column :color_products do |p|
			generate_links_for p.color_products, :admin_color_product_path, p.color_products.map { |x| x.colors.map { |y| y.name }.join(' - ') }
		end
		column :created_at
		default_actions
	end

	show do
		attributes_table do
			row :collection
			row :sub_ambient
			row :product_category
			row :name
			row :description
			row :specification
			row :price do |prod|
				prod.no_promotion_price
			end
			row :promotional_price
			row :parcels
			row :weight_in_grams
			row :height_in_centimeters
			row :width_in_centimeters
			row :depth_in_centimeters
			row :color_products do |p|
				generate_links_for p.color_products, :admin_color_product_path, p.color_products.map { |x| x.colors.map { |y| y.name }.join(' - ') }
			end
			row :spotlight
			row :magazine_description
		end
	end

	filter :ambient
	filter :product_category, collection: ProductCategory.order('name ASC')
	filter :name
	filter :price
end
