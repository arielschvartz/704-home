ActiveAdmin.register Press do
	menu parent: "Extras"

	config.clear_sidebar_sections!

	form do |f|
    f.inputs "Foto" do
      f.input :photo, hint: f.template.image_tag(f.object.photo.url(:thumb))
    end
    f.actions
  end
end
