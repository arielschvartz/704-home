ActiveAdmin.register PostCategory do
	menu parent: "Posts"
	
	show do
		attributes_table do
			row :name
			# row :products do |ambient|
			# 	generate_links_for ambient.products, :admin_product_path
			# end
		end
	end

	index do
		column :name
		# column :products do |ambient|
		# 	generate_links_for ambient.products, :admin_product_path
		# end
		default_actions
	end

	filter :name
end