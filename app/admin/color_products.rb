# encoding: UTF-8

ActiveAdmin.register ColorProduct do
	menu parent: "Produtos"

	form do |f|
		f.inputs "Detalhes da Cor do Produto" do
			f.input :product, collection: Product.order('name ASC')
			f.input :colors, as: :select, collection: options_from_collection_for_select(Color.order('name ASC'), "id", "name", f.object.colors.map { |x| x.id }), multiple: true
			f.input :stock_ammount
		end
		f.inputs "Fotos" do
			f.has_many :color_product_photos, allow_destroy: true do |cpf|
				cpf.input :photo, hint: f.template.image_tag(cpf.object.photo.url(:home_thumb))
				cpf.object.ambient_default ||= false
				cpf.input :ambient_default, label: "Foto padrão do ambiente"
				cpf.object.product_default ||= false
				cpf.input :product_default, label: "Foto padrão do produto"
				cpf.input :position, label: "Posição da foto"
			end
		end
		f.inputs "Produtos Relacionados" do
			f.input :follows, as: :select, collection: options_from_collection_for_select(ColorProduct.where("color_products.id != ?", (f.object.id or 0)).includes(:product).order('products.name ASC'), "id", "name", f.object.all_following.collect { |x| x.id }), multiple: true
		end
		f.actions
	end

	controller do
		def create
			follow_ids = params[:color_product].delete(:follow_ids)
			follow_ids.delete('')
			super
			create_follow @color_product, follow_ids
		end
		def update
			follow_ids = params[:color_product].delete(:follow_ids)
			follow_ids.delete('')
			super
			create_follow @color_product, follow_ids
		end
	end

	show do
		attributes_table do
			row :colors do |cp|
				generate_links_for cp.colors, :admin_color_path
			end
			row :product
			row :stock_ammount
			row :all_following do |cp|
				generate_links_for cp.all_following, :admin_color_product_path
			end
		end

		panel "Fotos" do
			if color_product.photos.present?
				show_photos color_product.photos, true
			end
		end
	end

	index do
		column :colors do |cp|
			generate_links_for cp.colors, :admin_color_path
		end
		column :product
		column :stock_ammount
		column :photos do |cp|
			if cp.photos.present?
				show_photos cp.photos
			end
		end
		column :all_following do |cp|
			generate_links_for cp.all_following, :admin_color_product_path
		end
		default_actions
	end

	filter :product, collection: Product.order('name ASC')
	filter :color
end

def show_photos photos, show_all = false
	unless photos.empty?
		render "shared/img", photos: photos, show_all: show_all
	end
end

def create_follow object, follow_ids
	if object.persisted? and object.valid?
		if follow_ids.present?
			follow_ids.each do |f|
				if Follow.where(followable_id: ColorProduct.find(f).id, follower_id: object.id).limit(1).empty?
					object.follow(ColorProduct.find(f))
				end
			end
		end
		Follow.where(follower_id: object.id).each do |prev_follow|
			unless follow_ids.include? prev_follow.followable_id.to_s
				prev_follow.destroy
			end
		end
	end
end
