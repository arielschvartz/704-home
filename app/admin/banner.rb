ActiveAdmin.register Banner do
  form do |f|
    f.inputs "Detalhes" do
      f.input :image
      f.input :url
      f.input :position
      f.input :active
    end

    f.actions
  end

  index do
    column :image
    column :url
    column :position
    column :active
    default_actions
  end

  filter :active
end
