# encoding: UTF-8

ActiveAdmin.register Magazine do
	form partial: 'form'

	controller do
		def new
			@color_products = ColorProduct.includes(:product).includes(:colors).all
			super
		end

		def create
			pages = set_pages_array params

			super

			create_pages_and_refs pages, @magazine
		end

		def edit
			@color_products = ColorProduct.includes(:product).includes(:colors).all
			super
		end

		def update
			pages = set_pages_array params, true

			super

			destroy_pages @magazine
			create_pages_and_refs pages, @magazine
		end
	end

	config.clear_sidebar_sections!

	show do
		attributes_table do
			row :collection
			row :id
			row :pdf_file
			# row :magazine_pages do |magazine|
			# 	show_photos magazine.magazine_pages.map { |x| x.photo }
			# end
		end

		panel "Páginas" do
			show_photos magazine.magazine_pages.map { |x| x.photo }, true
		end
	end

	index do
		column :id
		column :pdf_file
		column :magazine_pages do |magazine|
			show_photos magazine.magazine_pages.map { |x| x.photo }
		end
		default_actions
	end
end

def show_photos photos, show_all = false
	render "shared/img", photos: photos, show_all: show_all
end

def show_page form, pg = nil, i = nil
	render("shared/page", f: form, page: pg, i: i)
end

def set_pages_array params, update = false

	# params[:magazine].delete(:photo)
	params.delete(:photo)
	params[:magazine].delete(:product)
	params[:magazine].delete(:website)

	pages = []
	page_count = -1

	reference_count = 0

	prop_count = 1

	magazine_keys1 = params.keys.select { |x| x.include? 'product' or x.include? 'website' }
	magazine_keys2 = params[:magazine].keys.select { |x| x.include? 'product' or x.include? 'website' }
	magazine_values1 = magazine_keys1.map { |x| params.delete(x) }
	magazine_values2 = magazine_keys2.map { |x| params[:magazine].delete(x) }

	magazine_keys = magazine_keys1 + magazine_keys2
	magazine_values = magazine_values1 + magazine_values2


	params.select { |k, v| k.to_s.include? 'photo' }.each_with_index do |photo, i|
		pages[i] = { url: params.delete(photo[0]), references: [] }
	end

	new_magazine_keys = magazine_keys.map do |x|
		if x.include? 'product'
			"p#{x[7..-1]}"
		elsif x.include? 'website'
			"w#{x[7..-1]}"
		end
	end

	pages.each_with_index do |page, i|
		new_magazine_keys.each_with_index do |k, j|
			if k[1] == i.to_s
				pages[i][:references][k[2..-1].to_i] ||= {}
				pages[i][:references][k[2..-1].to_i][magazine_keys[j][0..6].to_sym] = magazine_values[j]
				pages[i][:references][k[2..-1].to_i][:pos_x] = params["posX#{k[1..-1]}"]
				pages[i][:references][k[2..-1].to_i][:pos_y] = params["posY#{k[1..-1]}"]
			end
		end
	end

	# params.each do |k, v|
	# 	if k == 'action' or k == 'commit'
	# 		break
	# 	elsif k.include? 'photo'
	# 		page_count += 1
	# 		pages[page_count] = {url: params.delete(k), references: []}
	# 		reference_count = 0
	# 	elsif page_count >= 0
	# 		pages[page_count][:references][reference_count] ||= {}
	# 		pages[page_count][:references][reference_count][k[0..3].to_sym] = params.delete(k)
	# 		if prop_count % 2 == 0
	# 			pages[page_count][:references][reference_count][:prod] = magazine_values.delete_at(0)
	# 			pages[page_count][:references][reference_count][:website] = magazine_values.delete_at(0)
	# 			reference_count += 1
	# 			prop_count = 1
	# 		else
	# 			prop_count += 1
	# 		end
	# 	end
	# end

	return pages
end

def create_pages_and_refs pages, magazine
	pages.each do |page|
		@page = magazine.magazine_pages.new(photo_file_name: page[:url])
		@page.save
		page[:references].each do |ref|
			id = nil
			id = ref[:product].to_i unless ref[:product].blank?
			@reference = @page.references.new(pos_x: ref[:pos_x], pos_y: ref[:pos_y], product_id: id, website: ref[:website])
			@reference.save
		end
	end
end

def destroy_pages magazine
	magazine.magazine_pages.each do |page|
		page.destroy
	end
end