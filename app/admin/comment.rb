ActiveAdmin.register Comment do
	menu parent: "Posts"

	actions :all, except: :new
	config.filters = false

	controller do
		def index
			if params[:scope].nil?
				params[:order] = "id_desc"
				params[:page] = "1"
				params[:scope] = "novo"
			end
			super
		end
	end

	scope :novo
	scope :aprovado
	scope :rejeitado

	index do
		column :name
		column :email
		column :content
		column :url
		column :approved
		default_actions
	end
end