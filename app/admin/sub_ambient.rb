ActiveAdmin.register SubAmbient do
	menu parent: "Produtos"
	
	show do
		attributes_table do
			row :name
			row :products do |sub_ambient|
				generate_links_for sub_ambient.products, :admin_product_path
			end
			row :ambient
		end
	end

	index do
		column :name
		column :products do |sub_ambient|
			generate_links_for sub_ambient.products, :admin_product_path
		end
		column :ambient
		default_actions
	end

	filter :name

end