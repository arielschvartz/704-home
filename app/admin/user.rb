ActiveAdmin.register User do
  actions :all, :except => [:new, :edit, :destroy]

  index do
    column :name
    column :email
    column :cpf
    default_actions
  end
end
