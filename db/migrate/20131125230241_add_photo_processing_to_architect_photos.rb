class AddPhotoProcessingToArchitectPhotos < ActiveRecord::Migration
  def change
    add_column :architect_photos, :photo_processing, :boolean
  end
end
