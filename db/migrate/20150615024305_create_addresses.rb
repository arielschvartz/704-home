class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :user
      t.boolean :billing, default: false
      t.boolean :shipping, default: false

      t.string :postal_code
      t.string :street
      t.string :number
      t.string :complement
      t.string :district
      t.string :city
      t.string :state
      t.string :country, default: 'BRA'
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :addresses, :user_id
    add_index :addresses, :deleted_at
  end
end
