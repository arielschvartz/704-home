class AddProjectPhotosToArchitect < ActiveRecord::Migration
  def change
    add_column :architects, :project_photos, :text
  end
end
