class CreateSubAmbients < ActiveRecord::Migration
  def change
    create_table :sub_ambients do |t|
      t.string :name
      t.integer :ambient_id

      t.timestamps
    end
  end
end
