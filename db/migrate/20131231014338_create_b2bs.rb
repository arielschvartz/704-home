class CreateB2bs < ActiveRecord::Migration
  def change
    create_table :b2bs do |t|
      t.string :brand_name
      t.string :business_name
      t.string :business_phone
      t.string :cnpj
      t.string :contact_email
      t.string :contact_name
      t.string :contact_phone

      t.timestamps
    end
  end
end
