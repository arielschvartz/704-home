class CreateCartColorProducts < ActiveRecord::Migration
  def change
    create_table :cart_color_products do |t|
      t.integer :cart_id
      t.integer :color_product_id
      t.integer :ammount
      t.decimal :unit_price

      t.timestamps
    end

    add_index :cart_color_products, :cart_id
  end
end
