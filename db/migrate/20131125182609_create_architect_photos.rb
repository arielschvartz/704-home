class CreateArchitectPhotos < ActiveRecord::Migration
  def change
    create_table :architect_photos do |t|
      t.integer :architect_id

      t.timestamps
    end

    add_index :architect_photos, :architect_id
    add_attachment :architect_photos, :photo
  end
end
