class AddLocalIdToColors < ActiveRecord::Migration
  def change
    add_column :colors, :local_id, :string
  end
end
