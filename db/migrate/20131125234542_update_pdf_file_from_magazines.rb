class UpdatePdfFileFromMagazines < ActiveRecord::Migration
  def up
  	remove_column :magazines, :pdf_file

  	add_attachment :magazines, :pdf_file
  end

  def down
  	add_columns :magazines, :pdf_file

  	remove_attachment :magazines, :pdf_file
  end
end
