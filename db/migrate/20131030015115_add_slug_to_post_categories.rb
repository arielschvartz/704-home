class AddSlugToPostCategories < ActiveRecord::Migration
  def change
    add_column :post_categories, :slug, :string
  end
end
