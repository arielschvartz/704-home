class AddMagazineDescriptionToProduct < ActiveRecord::Migration
  def change
    add_column :products, :magazine_description, :text
  end
end
