class AddB2bToExtraInfo < ActiveRecord::Migration
  def change
    add_column :extra_infos, :b2b, :text
  end
end
