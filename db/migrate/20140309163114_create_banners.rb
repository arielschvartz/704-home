class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.attachment :image
      t.string :url
      t.boolean :active, default: true
      t.integer :type, default: 1

      t.timestamps
    end

    add_index :banners, [:active, :type]
  end
end
