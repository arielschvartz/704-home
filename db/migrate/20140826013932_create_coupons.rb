class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.references :promotion
      t.string :code
      t.integer :maximum_uses

      t.timestamps
    end
    add_index :coupons, :promotion_id
  end
end
