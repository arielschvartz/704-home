class UpdatePhotos < ActiveRecord::Migration
  def up
  	remove_column :magazine_pages, :photo
  	remove_column :presses, :photo
  	remove_column :architects, :photo

  	add_attachment :magazine_pages, :photo
  	add_attachment :posts, :photo
  	add_attachment :presses, :photo
  	add_attachment :architects, :photo
  end

  def down
  	add_columns :magazine_pages, :photo
  	add_columns :presses, :photo
  	remove_column :architects, :photo

  	remove_attachment :magazine_pages, :photo
  	remove_attachment :posts, :photo
  	remove_attachment :presses, :photo
  	remove_attachment :architects, :photo
  end
end