class CreateExtraInfos < ActiveRecord::Migration
  def change
    create_table :extra_infos do |t|
      t.text :privacy_terms
      t.text :trade_terms
      t.text :club_704
      t.string :address
      t.string :phone
      t.string :cnpj
      t.string :business_name

      t.timestamps
    end
  end
end
