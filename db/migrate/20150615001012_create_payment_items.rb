class CreatePaymentItems < ActiveRecord::Migration
  def change
    create_table :payment_items do |t|
      t.references :payment
      t.integer :item_id
      t.string :item_description
      t.float :item_amount
      t.integer :item_quantity

      t.timestamps
    end
    add_index :payment_items, :payment_id
  end
end
