class AddAmbientToColorProductPhoto < ActiveRecord::Migration
  def change
    add_column :color_product_photos, :ambient_default, :boolean, default: false
    add_column :color_product_photos, :product_default, :boolean, default: false
  end
end
