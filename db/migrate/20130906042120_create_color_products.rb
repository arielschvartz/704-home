class CreateColorProducts < ActiveRecord::Migration
  def change
    create_table :color_products do |t|
      t.integer :color_id
      t.integer :product_id
      t.integer :stock_ammount
      t.text :photos
      t.string :slug

      t.timestamps
    end

    add_index :color_products, :product_id
    add_index :color_products, :stock_ammount
    add_index :color_products, :slug, unique: true
  end
end
