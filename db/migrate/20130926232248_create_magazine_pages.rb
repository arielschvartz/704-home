class CreateMagazinePages < ActiveRecord::Migration
  def self.up
    create_table :magazine_pages do |t|
      t.string :photo
      t.integer :magazine_id
      t.timestamps
    end
  end

  def self.down
    drop_table :magazine_pages
  end
end
