class CreateMultibrands < ActiveRecord::Migration
  def change
    create_table :multibrands do |t|
      t.string :contact_name
      t.string :contact_email
      t.string :contact_phone
      t.string :brand_name
      t.string :business_name
      t.string :cnpj
      t.string :address
      t.string :neghborhood
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :business_phone

      t.timestamps
    end
  end
end
