class AddShippingCostToCart < ActiveRecord::Migration
  def change
    add_column :carts, :shipping_cost, :integer
  end
end
