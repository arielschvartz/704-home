class AddGiftToCart < ActiveRecord::Migration
  def change
    add_column :carts, :gift, :boolean, default: false
  end
end
