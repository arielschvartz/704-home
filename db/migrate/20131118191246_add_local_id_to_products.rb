class AddLocalIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :local_id, :string
  end
end
