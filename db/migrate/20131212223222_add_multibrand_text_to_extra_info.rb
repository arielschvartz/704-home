class AddMultibrandTextToExtraInfo < ActiveRecord::Migration
  def change
    add_column :extra_infos, :multibrand, :text
  end
end
