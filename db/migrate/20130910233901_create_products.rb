class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :collection_id
      t.integer :ambient_id
      t.integer :product_category_id
      t.string :name
      t.text :description
      t.text :specification
      t.decimal :price

      t.timestamps
    end
  end
end
