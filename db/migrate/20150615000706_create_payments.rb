class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :cart, index: true
      t.references :user, index: true
      t.string :payment_mode
      t.string :payment_method
      t.string :currency
      t.float :extra_amount, default: 0.0
      t.string :notification_url
      t.string :redirect_url
      t.string :reference
      t.string :credit_card_token
      t.integer :installment_quantity
      t.integer :no_interest_installment_quantity
      t.float :installment_value
      t.string :credit_card_holder_name
      t.string :credit_card_holder_cpf
      t.string :credit_card_holder_birth_date
      t.string :credit_card_holder_area_code
      t.string :credit_card_holder_phone
      t.string :billing_address_street
      t.string :billing_address_number
      t.string :billing_address_complement
      t.string :billing_address_district
      t.string :billing_address_postal_code
      t.string :billing_address_city
      t.string :billing_address_state
      t.string :billing_address_country, default: 'BRA'
      t.string :shipping_address_street
      t.string :shipping_address_number
      t.string :shipping_address_complement
      t.string :shipping_address_district
      t.string :shipping_address_postal_code
      t.string :shipping_address_city
      t.string :shipping_address_state
      t.string :shipping_address_country, default: 'BRA'
      t.string :shipping_type
      t.float :shipping_cost
      t.string :sender_name
      t.string :sender_cpf
      t.string :sender_area_code
      t.string :sender_phone
      t.string :sender_email
      t.string :sender_hash

      t.text :response

      t.timestamps
    end
  end
end
