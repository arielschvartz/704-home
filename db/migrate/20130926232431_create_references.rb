class CreateReferences < ActiveRecord::Migration
  def self.up
    create_table :references do |t|
      t.float :pos_x
      t.float :pos_y
      t.integer :product_id
      t.integer :magazine_page_id
      t.timestamps
    end
  end

  def self.down
    drop_table :references
  end
end
