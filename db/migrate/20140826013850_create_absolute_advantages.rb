class CreateAbsoluteAdvantages < ActiveRecord::Migration
  def change
    create_table :absolute_advantages do |t|
      t.references :promotion, index: true
      t.decimal :amount

      t.timestamps
    end
  end
end
