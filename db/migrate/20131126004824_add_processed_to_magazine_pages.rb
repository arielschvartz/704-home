class AddProcessedToMagazinePages < ActiveRecord::Migration
  def change
    add_column :magazine_pages, :processed, :boolean, default: false, null: false
    add_index :magazine_pages, :processed
  end
end
