class AddAboutToExtraInfo < ActiveRecord::Migration
  def change
    add_column :extra_infos, :about, :text
  end
end
