class AddPhotoProcessingToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :photo_processing, :boolean
  end
end
