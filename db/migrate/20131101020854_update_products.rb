class UpdateProducts < ActiveRecord::Migration
  def up
  	rename_column :products, :ambient_id, :sub_ambient_id
  end

  def down
  	rename_column :products, :sub_ambient_id, :ambient_id
  end
end
