class AddSlugToSubAmbient < ActiveRecord::Migration
  def change
    add_column :sub_ambients, :slug, :string
  end
end
