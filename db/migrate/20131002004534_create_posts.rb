class CreatePosts < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.string :author
      t.datetime :date
      t.string :title
      t.integer :post_category_id
      t.text :content
      t.string :media_link
      t.string :media_type
      t.integer :architect_id
      t.string :slug
      t.timestamps
    end
  end

  def self.down
    drop_table :posts
  end
end
