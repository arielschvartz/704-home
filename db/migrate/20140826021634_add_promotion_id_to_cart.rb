class AddPromotionIdToCart < ActiveRecord::Migration
  def change
    add_column :carts, :promotion_id, :integer
    add_index :carts, :promotion_id
  end
end
