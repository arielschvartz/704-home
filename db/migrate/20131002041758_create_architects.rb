class CreateArchitects < ActiveRecord::Migration
  def self.up
    create_table :architects do |t|
      t.string :name
      t.text :description
      t.string :photo
      t.timestamps
    end
  end

  def self.down
    drop_table :architects
  end
end
