class CreateAmbients < ActiveRecord::Migration
  def self.up
    create_table :ambients do |t|
      t.string :name
      t.timestamps
    end
  end

  def self.down
    drop_table :ambients
  end
end
