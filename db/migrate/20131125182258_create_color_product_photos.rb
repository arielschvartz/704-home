class CreateColorProductPhotos < ActiveRecord::Migration
  def change
    create_table :color_product_photos do |t|
      t.integer :color_product_id

      t.timestamps
    end

    add_index :color_product_photos, :color_product_id
    add_attachment :color_product_photos, :photo
    remove_column :color_products, :photos
  end
end