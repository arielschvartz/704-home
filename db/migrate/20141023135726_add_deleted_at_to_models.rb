class AddDeletedAtToModels < ActiveRecord::Migration
  def change
    add_column :products, :deleted_at, :datetime
    add_index :products, :deleted_at

    add_column :color_products, :deleted_at, :datetime
    add_index :color_products, :deleted_at
  end
end
