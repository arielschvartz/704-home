class AddTransactionToPaymentNotifications < ActiveRecord::Migration
  def change
    add_column :payment_notifications, :transaction_content, :text
    add_column :payment_notifications, :payment_method_code, :string
  end
end
