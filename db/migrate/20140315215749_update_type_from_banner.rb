class UpdateTypeFromBanner < ActiveRecord::Migration
  def change
    rename_column :banners, :type, :banner_style
  end
end
