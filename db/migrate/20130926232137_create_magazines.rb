class CreateMagazines < ActiveRecord::Migration
  def self.up
    create_table :magazines do |t|
      t.string :pdf_file
      t.integer :collection_id
      t.timestamps
    end
  end

  def self.down
    drop_table :magazines
  end
end
