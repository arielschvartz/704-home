class AddWebsiteToReference < ActiveRecord::Migration
  def change
    add_column :references, :website, :string
    add_column :references, :type_website, :boolean, default: false
  end
end
