class AddDeliveryTimeOutOfStateToExtraInfo < ActiveRecord::Migration
  def change
    add_column :extra_infos, :delivery_time_out_of_state, :integer
  end
end
