class AddSlugToArchitects < ActiveRecord::Migration
  def change
    add_column :architects, :slug, :string
  end
end
