class AddPositionToColorProductPhoto < ActiveRecord::Migration
  def change
    add_column :color_product_photos, :position, :integer, default: 0
  end
end
