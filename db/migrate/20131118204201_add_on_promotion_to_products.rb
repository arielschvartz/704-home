class AddOnPromotionToProducts < ActiveRecord::Migration
  def change
    add_column :products, :on_promotion, :boolean, default: false
  end
end
