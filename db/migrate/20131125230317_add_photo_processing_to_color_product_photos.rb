class AddPhotoProcessingToColorProductPhotos < ActiveRecord::Migration
  def change
    add_column :color_product_photos, :photo_processing, :boolean
  end
end
