class UpdateColor < ActiveRecord::Migration
  def up
  	remove_column :color_products, :color_id
  end

  def down
  	add_column :color_products, :color_id, :integer
  end
end
