class AddShippingCodeToCart < ActiveRecord::Migration
  def change
    add_column :carts, :shipping_code, :string
  end
end
