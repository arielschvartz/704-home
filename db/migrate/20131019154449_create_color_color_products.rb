class CreateColorColorProducts < ActiveRecord::Migration
  def change
    create_table :color_products_colors do |t|
      t.integer :color_id
      t.integer :color_product_id
    end
  end
end