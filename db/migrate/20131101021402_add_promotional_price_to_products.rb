class AddPromotionalPriceToProducts < ActiveRecord::Migration
  def change
    add_column :products, :promotional_price, :float
    add_column :products, :parcels, :integer
  end
end
