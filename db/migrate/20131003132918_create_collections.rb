class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.string :name
      t.date :initial_date
      t.date :end_date

      t.timestamps
    end
  end
end
