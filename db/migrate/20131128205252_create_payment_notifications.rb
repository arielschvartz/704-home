class CreatePaymentNotifications < ActiveRecord::Migration
  def change
    create_table :payment_notifications do |t|
      t.string :code
      t.integer :cart_id
      t.string :status
      t.string :user_phone
      t.string :user_name
      t.string :user_email
      t.string :user_cpf
      t.string :payment_method
      t.string :gross
      t.string :parcels
      t.string :street
      t.string :number
      t.string :complement
      t.string :neighborhood
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :country
      t.string :shipping_method
      t.string :shipping_cost
      t.string :date
      t.string :type_of_transaction
      t.string :discount_ammount
      t.string :fee_ammount
      t.string :net_ammount
      t.string :escrow_end_date
      t.string :extra_ammount
      t.boolean :syncronized

      t.timestamps
    end

    add_index :payment_notifications, :cart_id
    add_index :payment_notifications, :syncronized
  end
end
