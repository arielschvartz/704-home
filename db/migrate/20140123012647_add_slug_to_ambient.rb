class AddSlugToAmbient < ActiveRecord::Migration
  def change
    add_column :ambients, :slug, :string
  end
end
