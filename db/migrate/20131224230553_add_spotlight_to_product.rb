class AddSpotlightToProduct < ActiveRecord::Migration
  def change
    add_column :products, :spotlight, :boolean, default: false
  end
end
