class AddShippingInfoToProducts < ActiveRecord::Migration
  def change
    add_column :products, :weight_in_grams, :integer, default: 500
    add_column :products, :height_in_centimeters, :integer, default: 3
    add_column :products, :width_in_centimeters, :integer, default: 12
    add_column :products, :depth_in_centimeters, :integer, default: 16
  end
end
