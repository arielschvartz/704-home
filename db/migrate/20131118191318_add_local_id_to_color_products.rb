class AddLocalIdToColorProducts < ActiveRecord::Migration
  def change
    add_column :color_products, :local_id, :string
  end
end
