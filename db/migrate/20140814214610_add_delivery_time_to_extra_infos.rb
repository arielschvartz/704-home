class AddDeliveryTimeToExtraInfos < ActiveRecord::Migration
  def change
    add_column :extra_infos, :delivery_time, :integer
  end
end
