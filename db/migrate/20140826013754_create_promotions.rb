class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.date :start_date
      t.date :end_date
      t.integer :uses, default: 0
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
