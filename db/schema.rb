# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20151011194127) do

  create_table "absolute_advantages", :force => true do |t|
    t.integer  "promotion_id"
    t.decimal  "amount"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "addresses", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "billing",     :default => false
    t.boolean  "shipping",    :default => false
    t.string   "postal_code"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "district"
    t.string   "city"
    t.string   "state"
    t.string   "country",     :default => "BRA"
    t.datetime "deleted_at"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "addresses", ["deleted_at"], :name => "index_addresses_on_deleted_at"
  add_index "addresses", ["user_id"], :name => "index_addresses_on_user_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "ambients", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  create_table "architect_photos", :force => true do |t|
    t.integer  "architect_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "photo_processing"
  end

  add_index "architect_photos", ["architect_id"], :name => "index_architect_photos_on_architect_id"

  create_table "architects", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "slug"
    t.text     "project_photos"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "b2bs", :force => true do |t|
    t.string   "brand_name"
    t.string   "business_name"
    t.string   "business_phone"
    t.string   "cnpj"
    t.string   "contact_email"
    t.string   "contact_name"
    t.string   "contact_phone"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "banners", :force => true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "url"
    t.boolean  "active",             :default => true
    t.integer  "banner_style",       :default => 1
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.integer  "position"
  end

  add_index "banners", ["active", "banner_style"], :name => "index_banners_on_active_and_type"

  create_table "cart_color_products", :force => true do |t|
    t.integer  "cart_id"
    t.integer  "color_product_id"
    t.integer  "ammount"
    t.decimal  "unit_price"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "cart_color_products", ["cart_id"], :name => "index_cart_color_products_on_cart_id"

  create_table "carts", :force => true do |t|
    t.integer  "user_id"
    t.datetime "payment_date"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.boolean  "gift",          :default => false
    t.string   "shipping_code"
    t.integer  "promotion_id"
    t.integer  "shipping_cost"
  end

  add_index "carts", ["promotion_id"], :name => "index_carts_on_promotion_id"
  add_index "carts", ["user_id"], :name => "index_carts_on_user_id"

  create_table "collections", :force => true do |t|
    t.string   "name"
    t.date     "initial_date"
    t.date     "end_date"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "slug"
  end

  create_table "color_product_photos", :force => true do |t|
    t.integer  "color_product_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "photo_processing"
    t.boolean  "ambient_default",    :default => false
    t.boolean  "product_default",    :default => false
    t.integer  "position",           :default => 0
  end

  add_index "color_product_photos", ["color_product_id"], :name => "index_color_product_photos_on_color_product_id"

  create_table "color_products", :force => true do |t|
    t.integer  "product_id"
    t.integer  "stock_ammount"
    t.string   "slug"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "local_id"
    t.datetime "deleted_at"
  end

  add_index "color_products", ["deleted_at"], :name => "index_color_products_on_deleted_at"
  add_index "color_products", ["product_id"], :name => "index_color_products_on_product_id"
  add_index "color_products", ["slug"], :name => "index_color_products_on_slug", :unique => true
  add_index "color_products", ["stock_ammount"], :name => "index_color_products_on_stock_ammount"

  create_table "color_products_colors", :force => true do |t|
    t.integer "color_id"
    t.integer "color_product_id"
  end

  create_table "colors", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "local_id"
  end

  add_index "colors", ["code"], :name => "index_colors_on_code"

  create_table "comments", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "content"
    t.string   "url"
    t.boolean  "approved"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "coupons", :force => true do |t|
    t.integer  "promotion_id"
    t.string   "code"
    t.integer  "maximum_uses"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "coupons", ["promotion_id"], :name => "index_coupons_on_promotion_id"

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "extra_infos", :force => true do |t|
    t.text     "privacy_terms"
    t.text     "trade_terms"
    t.text     "club_704"
    t.string   "address"
    t.string   "phone"
    t.string   "cnpj"
    t.string   "business_name"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.text     "about"
    t.text     "multibrand"
    t.text     "b2b"
    t.integer  "delivery_time"
    t.integer  "delivery_time_out_of_state"
  end

  create_table "faqs", :force => true do |t|
    t.string   "question"
    t.text     "answer"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "follows", :force => true do |t|
    t.integer  "followable_id",                      :null => false
    t.string   "followable_type",                    :null => false
    t.integer  "follower_id",                        :null => false
    t.string   "follower_type",                      :null => false
    t.boolean  "blocked",         :default => false, :null => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "follows", ["followable_id", "followable_type"], :name => "fk_followables"
  add_index "follows", ["follower_id", "follower_type"], :name => "fk_follows"

  create_table "magazine_pages", :force => true do |t|
    t.integer  "magazine_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "processed",          :default => false, :null => false
  end

  add_index "magazine_pages", ["processed"], :name => "index_magazine_pages_on_processed"

  create_table "magazines", :force => true do |t|
    t.integer  "collection_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "pdf_file_file_name"
    t.string   "pdf_file_content_type"
    t.integer  "pdf_file_file_size"
    t.datetime "pdf_file_updated_at"
  end

  create_table "multibrands", :force => true do |t|
    t.string   "contact_name"
    t.string   "contact_email"
    t.string   "contact_phone"
    t.string   "brand_name"
    t.string   "business_name"
    t.string   "cnpj"
    t.string   "address"
    t.string   "neghborhood"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "business_phone"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "newsletters", :force => true do |t|
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "payment_items", :force => true do |t|
    t.integer  "payment_id"
    t.integer  "item_id"
    t.string   "item_description"
    t.float    "item_amount"
    t.integer  "item_quantity"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "payment_items", ["payment_id"], :name => "index_payment_items_on_payment_id"

  create_table "payment_notifications", :force => true do |t|
    t.string   "code"
    t.integer  "cart_id"
    t.string   "status"
    t.string   "user_phone"
    t.string   "user_name"
    t.string   "user_email"
    t.string   "user_cpf"
    t.string   "payment_method"
    t.string   "gross"
    t.string   "parcels"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "country"
    t.string   "shipping_method"
    t.string   "shipping_cost"
    t.string   "date"
    t.string   "type_of_transaction"
    t.string   "discount_ammount"
    t.string   "fee_ammount"
    t.string   "net_ammount"
    t.string   "escrow_end_date"
    t.string   "extra_ammount"
    t.boolean  "syncronized"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.text     "transaction_content"
    t.string   "payment_method_code"
  end

  add_index "payment_notifications", ["cart_id"], :name => "index_payment_notifications_on_cart_id"
  add_index "payment_notifications", ["syncronized"], :name => "index_payment_notifications_on_syncronized"

  create_table "payments", :force => true do |t|
    t.integer  "cart_id"
    t.integer  "user_id"
    t.string   "payment_mode"
    t.string   "payment_method"
    t.string   "currency"
    t.float    "extra_amount",                     :default => 0.0
    t.string   "notification_url"
    t.string   "redirect_url"
    t.string   "reference"
    t.string   "credit_card_token"
    t.integer  "installment_quantity"
    t.integer  "no_interest_installment_quantity"
    t.float    "installment_value"
    t.string   "credit_card_holder_name"
    t.string   "credit_card_holder_cpf"
    t.string   "credit_card_holder_birth_date"
    t.string   "credit_card_holder_area_code"
    t.string   "credit_card_holder_phone"
    t.string   "billing_address_street"
    t.string   "billing_address_number"
    t.string   "billing_address_complement"
    t.string   "billing_address_district"
    t.string   "billing_address_postal_code"
    t.string   "billing_address_city"
    t.string   "billing_address_state"
    t.string   "billing_address_country",          :default => "BRA"
    t.string   "shipping_address_street"
    t.string   "shipping_address_number"
    t.string   "shipping_address_complement"
    t.string   "shipping_address_district"
    t.string   "shipping_address_postal_code"
    t.string   "shipping_address_city"
    t.string   "shipping_address_state"
    t.string   "shipping_address_country",         :default => "BRA"
    t.string   "shipping_type"
    t.float    "shipping_cost"
    t.string   "sender_name"
    t.string   "sender_cpf"
    t.string   "sender_area_code"
    t.string   "sender_phone"
    t.string   "sender_email"
    t.string   "sender_hash"
    t.text     "response"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

  create_table "phones", :force => true do |t|
    t.integer  "user_id"
    t.string   "area_code"
    t.string   "number"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "phones", ["user_id"], :name => "index_phones_on_user_id"

  create_table "post_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  create_table "posts", :force => true do |t|
    t.string   "author"
    t.datetime "date"
    t.string   "title"
    t.integer  "post_category_id"
    t.text     "content"
    t.string   "media_link"
    t.string   "media_type"
    t.integer  "architect_id"
    t.string   "slug"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "photo_processing"
  end

  create_table "presses", :force => true do |t|
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "product_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  create_table "products", :force => true do |t|
    t.integer  "collection_id"
    t.integer  "sub_ambient_id"
    t.integer  "product_category_id"
    t.string   "name"
    t.text     "description"
    t.text     "specification"
    t.decimal  "price"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.float    "promotional_price"
    t.integer  "parcels"
    t.string   "local_id"
    t.boolean  "on_promotion",          :default => false
    t.boolean  "spotlight",             :default => false
    t.text     "magazine_description"
    t.datetime "deleted_at"
    t.integer  "weight_in_grams",       :default => 500
    t.integer  "height_in_centimeters", :default => 3
    t.integer  "width_in_centimeters",  :default => 12
    t.integer  "depth_in_centimeters",  :default => 16
  end

  add_index "products", ["deleted_at"], :name => "index_products_on_deleted_at"

  create_table "promotions", :force => true do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "uses",       :default => 0
    t.boolean  "active",     :default => true
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "references", :force => true do |t|
    t.float    "pos_x"
    t.float    "pos_y"
    t.integer  "product_id"
    t.integer  "magazine_page_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "website"
    t.boolean  "type_website",     :default => false
  end

  create_table "sub_ambients", :force => true do |t|
    t.string   "name"
    t.integer  "ambient_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  create_table "testimonials", :force => true do |t|
    t.string   "name"
    t.string   "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "provider"
    t.string   "uid"
    t.string   "gender"
    t.date     "birthday"
    t.string   "location"
    t.string   "relationship_status"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "cpf"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["uid"], :name => "index_users_on_uid", :unique => true

end
